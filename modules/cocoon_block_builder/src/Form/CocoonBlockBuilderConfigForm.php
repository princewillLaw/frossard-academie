<?php

/**
 * @file
 * Contains CocoonBlockBuilderConfigForm.
 */

namespace Drupal\cocoon_block_builder\Form;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

class CocoonBlockBuilderConfigForm extends FormBase {

  /**
   * @inheritdoc
   */
  public function getFormId() {
    return 'cocoon_block_builder_config_form';
  }

  /**
   * @inheritdoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $entity_field_manager = \Drupal::service('entity_field.manager');

    $default_value = array();
    $options = array();
    foreach (\Drupal\node\Entity\NodeType::loadMultiple() as $node_type) {
      $options[$node_type->id()] = $node_type->label();

      if (in_array(COCOON_BLOCK_BUILDER_FIELD_NAME, array_keys($entity_field_manager->getFieldDefinitions('node', $node_type->id())))) {
        $default_value[] = $node_type->id();
      }
    }

    $form['enable'] = array(
      '#type' => 'checkboxes',
      '#title' => $this->t('Enable on'),
      '#options' => $options,
      '#default_value' => $default_value,
    );

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    );

    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $field_storage = FieldStorageConfig::loadByName('node', COCOON_BLOCK_BUILDER_FIELD_NAME);

    if (!$field_storage) {
      $field_storage = FieldStorageConfig::create(array(
        'field_name' => COCOON_BLOCK_BUILDER_FIELD_NAME,
        'type' => 'cocoon_block_reference',
        'entity_type' => 'node',
        'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
        'locked' => TRUE,
      ));
      $field_storage->save();
    }

    foreach ($form_state->getValue('enable') as $node_type => $enable) {
      $field = FieldConfig::loadByName('node', $node_type, COCOON_BLOCK_BUILDER_FIELD_NAME);

      if ($enable) {
        if (empty($field)) {
          $field = FieldConfig::create(array(
            'field_storage' => $field_storage,
            'bundle' => $node_type,
            'label' => 'Cocoon Block Builder',
            'field_name' => COCOON_BLOCK_BUILDER_FIELD_NAME,
            'entity_type' => 'node',
          ));
          $field->save();

          // Assign widget settings for the 'default' form mode.
          entity_get_form_display('node', $node_type, 'default')
            ->setComponent(COCOON_BLOCK_BUILDER_FIELD_NAME, array(
              'type' => 'cocoon_block_reference',
            ))
            ->save();

          // Assign display settings for the 'default' view mode.
          entity_get_display('node', $node_type, 'default')
            ->setComponent(COCOON_BLOCK_BUILDER_FIELD_NAME, array(
              'label' => 'hidden',
              'type' => 'cocoon_block_reference_default',
            ))
            ->save();
        }
      }
      else {
        if ($field) {
          $field->delete();
        }
      }
    }

    drupal_set_message($this->t('Configuration has been updated.'));
  }
}
