<?php

/**
 * @file
 * Contains CocoonBlockReferenceDefaultWidget.
 */

namespace Drupal\cocoon_block_builder\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the "cocoon_block_reference" widget.
 *
 * @FieldWidget(
 *   id = "cocoon_block_reference",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "cocoon_block_reference"
 *   }
 * )
 */
class CocoonBlockReferenceDefaultWidget extends EntityReferenceAutocompleteWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, array('#weight' => 0) + $element, $form, $form_state);

    $default_value = $items->get($delta)->getValue();

    $element['css_class'] = array(
      '#type' => 'textfield',
      '#placeholder' => $this->t('CSS class'),
      '#default_value' => isset($default_value['css_class']) ? $default_value['css_class'] : NULL,
      '#attached' => array(
        'library' => array(
          'cocoon_block_builder/cocoon-block-builder-customizations',
        )
      ),
    );

    $element['html_id'] = array(
      '#type' => 'textfield',
      '#placeholder' => $this->t('HTML ID'),
      '#default_value' => isset($default_value['html_id']) ? $default_value['html_id'] : NULL,
    );

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function form(FieldItemListInterface $items, array &$form, FormStateInterface $form_state, $get_delta = NULL) {
    $elements = parent::form($items, $form, $form_state, $get_delta);
    /*$elements['opening'] = array(
      '#theme' => 'image',
      '#uri' => 'public://cocoon-block-builder/opening.png',
      '#attributes' => array(
        'class' => array('cocoon-block-builder-opening'),
      ),
      '#weight' => -100,
    );*/
    return $elements;
  }
}
