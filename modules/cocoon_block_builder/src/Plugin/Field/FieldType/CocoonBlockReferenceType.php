<?php

/**
 * @file
 * Contains CocoonBlockReferenceType.
 */

namespace Drupal\cocoon_block_builder\Plugin\Field\FieldType;

use \Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use \Drupal\Core\StringTranslation\TranslatableMarkup;
use \Drupal\Core\TypedData\DataReferenceTargetDefinition;

/**
 * Provides a field type "cocoon block reference".
 *
 * @FieldType(
 *   id = "cocoon_block_reference",
 *   label = @Translation("Cocoon block reference"),
 *   default_widget = "cocoon_block_reference",
 *   default_formatter = "cocoon_block_reference_default",
 *   list_class = "\Drupal\cocoon_block_builder\Field\CocoonBlockBuilderFieldItemList",
 * )
 */
class CocoonBlockReferenceType extends EntityReferenceItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return array(
      'target_type' => 'block',
    ) + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);
    $schema['columns']['css_class'] = array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
      'description' => 'CSS class to wrap the block in.',
    );
    $schema['columns']['html_id'] = array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => TRUE,
      'default' => '',
      'description' => 'HTML ID to assign to the block wrapper.',
    );

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['css_class'] = DataReferenceTargetDefinition::create('string')
      ->setLabel(new TranslatableMarkup('CSS class'));

    $properties['html_id'] = DataReferenceTargetDefinition::create('string')
      ->setLabel(new TranslatableMarkup('HTML ID'));

    return $properties;
  }
}
