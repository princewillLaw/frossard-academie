<?php

/**
 * @file
 * Contains CocoonBlockReferenceDefaultFormatter
 */

namespace Drupal\cocoon_block_builder\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;

/**
 * Plugin implementation of the 'cocoon_block_reference' formatter.
 *
 * @FieldFormatter(
 *   id = "cocoon_block_reference_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "cocoon_block_reference"
 *   }
 * )
 */
class CocoonBlockReferenceDefaultFormatter extends EntityReferenceEntityFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    foreach ($elements as $delta => $element) {
      $values = $items->get($delta)->getValue();

      $elements[$delta] = array(
        '#theme_wrappers' => array('container'),
        '#attributes' => array(
          'class' => array('cocoon-block', $values['css_class']),
        ),
      ) + array($delta => $element);
      if ($values['html_id']) {
        $elements[$delta]['#attributes']['id'] = $values['html_id'];
      }
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity) {
    $entity->setStatus(TRUE);
    return parent::checkAccess($entity);
  }
}
