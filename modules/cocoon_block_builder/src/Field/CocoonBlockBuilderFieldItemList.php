<?php

/**
 * @contains
 * CocoonBlockBuilderFieldItemList
 */

namespace Drupal\cocoon_block_builder\Field;

use Drupal\Core\Field\EntityReferenceFieldItemList;

class CocoonBlockBuilderFieldItemList extends EntityReferenceFieldItemList {

  /**
   * {@inheritdoc}
   */
  public function referencedEntities() {
    $referenced_entities = parent::referencedEntities();
    foreach ($referenced_entities as $entity) {
      $entity->setStatus(TRUE);
    }
    return $referenced_entities;
  }

}
