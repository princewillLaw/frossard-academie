<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/appal/templates/paragraphs/paragraph--testimonial.html.twig */
class __TwigTemplate_47094abeb9d491494894a1416addaca2c64ac2e73cc6eeb6ecb9c4cd30ee9680 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'paragraph' => [$this, 'block_paragraph'],
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 2, "block" => 9];
        $filters = ["clean_class" => 4, "escape" => 13];
        $functions = ["file_url" => 13];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'block'],
                ['clean_class', 'escape'],
                ['file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        $context["classes"] = [0 => "paragraph", 1 => ("paragraph--type--" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed($this->getAttribute(        // line 4
($context["paragraph"] ?? null), "bundle", [])))), 2 => ((        // line 5
($context["view_mode"] ?? null)) ? (("paragraph--view-mode--" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(($context["view_mode"] ?? null))))) : ("")), 3 => (( !$this->getAttribute(        // line 6
($context["paragraph"] ?? null), "isPublished", [], "method")) ? ("paragraph--unpublished") : (""))];
        // line 9
        $this->displayBlock('paragraph', $context, $blocks);
    }

    public function block_paragraph($context, array $blocks = [])
    {
        // line 10
        echo "  ";
        $this->displayBlock('content', $context, $blocks);
    }

    public function block_content($context, array $blocks = [])
    {
        // line 11
        echo "    <div class=\"item clearfix\">
      <div class=\"hero-image\">
        <img src=\"";
        // line 13
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
        <span class=\"icon\" data-aos=\"zoom-in\" data-aos-duration=\"450\">
          <i class=\"flaticon-quotation\"></i>
        </span>
        <small class=\"design-image\">
          <img src=\"";
        // line 18
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/testimonial/design-image-1.png\" alt=\"image_not_found\">
        </small>
      </div>
      <div class=\"testimonial-content\">
        <div class=\"hero-info mb-60\">
          <h4 class=\"hero-name\">";
        // line 23
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_title", [])), "html", null, true);
        echo "</h4>
          <span class=\"hero-title\">";
        // line 24
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_title_2", [])), "html", null, true);
        echo "</span>
          <div class=\"rating-star ul-li clearfix\">
            <ul class=\"clearfix\">
              <li class=\"rated\">
                <i class=\"fas fa-star\"></i>
              </li>
              <li class=\"rated\">
                <i class=\"fas fa-star\"></i>
              </li>
              <li class=\"rated\">
                <i class=\"fas fa-star\"></i>
              </li>
              <li class=\"rated\">
                <i class=\"fas fa-star\"></i>
              </li>
              <li class=\"rated\">
                <i class=\"fas fa-star\"></i>
              </li>
            </ul>
          </div>
        </div>
        <p class=\"paragraph-text mb-0\">
          ";
        // line 46
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_body_plain", [])), "html", null, true);
        echo "
        </p>
      </div>
    </div>
  ";
    }

    public function getTemplateName()
    {
        return "themes/appal/templates/paragraphs/paragraph--testimonial.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 46,  99 => 24,  95 => 23,  87 => 18,  79 => 13,  75 => 11,  68 => 10,  62 => 9,  60 => 6,  59 => 5,  58 => 4,  57 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{%
  set classes = [
    'paragraph',
    'paragraph--type--' ~ paragraph.bundle|clean_class,
    view_mode ? 'paragraph--view-mode--' ~ view_mode|clean_class,
    not paragraph.isPublished() ? 'paragraph--unpublished'
  ]
%}
{% block paragraph %}
  {% block content %}
    <div class=\"item clearfix\">
      <div class=\"hero-image\">
        <img src=\"{{ file_url(content.field_image[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
        <span class=\"icon\" data-aos=\"zoom-in\" data-aos-duration=\"450\">
          <i class=\"flaticon-quotation\"></i>
        </span>
        <small class=\"design-image\">
          <img src=\"{{ base_path ~ directory }}/images/testimonial/design-image-1.png\" alt=\"image_not_found\">
        </small>
      </div>
      <div class=\"testimonial-content\">
        <div class=\"hero-info mb-60\">
          <h4 class=\"hero-name\">{{ content.field_title }}</h4>
          <span class=\"hero-title\">{{ content.field_title_2 }}</span>
          <div class=\"rating-star ul-li clearfix\">
            <ul class=\"clearfix\">
              <li class=\"rated\">
                <i class=\"fas fa-star\"></i>
              </li>
              <li class=\"rated\">
                <i class=\"fas fa-star\"></i>
              </li>
              <li class=\"rated\">
                <i class=\"fas fa-star\"></i>
              </li>
              <li class=\"rated\">
                <i class=\"fas fa-star\"></i>
              </li>
              <li class=\"rated\">
                <i class=\"fas fa-star\"></i>
              </li>
            </ul>
          </div>
        </div>
        <p class=\"paragraph-text mb-0\">
          {{ content.field_body_plain }}
        </p>
      </div>
    </div>
  {% endblock %}
{% endblock paragraph %}
", "themes/appal/templates/paragraphs/paragraph--testimonial.html.twig", "/home/princewill/Projects/Drupal Projects/drupal/themes/appal/templates/paragraphs/paragraph--testimonial.html.twig");
    }
}
