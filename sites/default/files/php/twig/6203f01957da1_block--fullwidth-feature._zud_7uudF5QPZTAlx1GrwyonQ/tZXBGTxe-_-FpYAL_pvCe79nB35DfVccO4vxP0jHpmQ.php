<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/appal/templates/blocks/cocoon/block--fullwidth-feature.html.twig */
class __TwigTemplate_ffa737e924a00cc662cf42b8e6785c1f1a31db3c5de928c25c89499367cb9db6 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 2, "block" => 11, "if" => 12];
        $filters = ["clean_class" => 4, "escape" => 10, "render" => 12];
        $functions = ["file_url" => 69];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'block', 'if'],
                ['clean_class', 'escape', 'render'],
                ['file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        $context["classes"] = [0 => "block", 1 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed($this->getAttribute(        // line 4
($context["configuration"] ?? null), "provider", [])))), 2 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 5
($context["plugin_id"] ?? null)))), 3 => ((        // line 6
($context["label"] ?? null)) ? ("has-title") : ("")), 4 => (($this->getAttribute($this->getAttribute(        // line 7
($context["content"] ?? null), "body", [], "array"), "#bundle", [], "array")) ? (("bundle-" . $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["content"] ?? null), "body", [], "array"), "#bundle", [], "array")))) : (""))];
        // line 10
        echo "<div";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
        echo ">
  ";
        // line 11
        $this->displayBlock('content', $context, $blocks);
        // line 100
        echo "</div>
";
    }

    // line 11
    public function block_content($context, array $blocks = [])
    {
        // line 12
        echo "    <section id=\"features-section\" class=\"";
        if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_image_2", []))) {
            echo " features-section  ";
        } else {
            echo " app-section  ";
        }
        echo " sec-ptb-160 clearfix ccn-fullwidth-feature\">
      <div class=\"container\">
        <div class=\"feature-item\">
          <div class=\"row justify-content-lg-between justify-content-md-center ";
        // line 15
        if (twig_in_filter("1", $this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_text_align", []), 0, [], "array"))) {
            echo " ccn-row-reverse ";
        }
        echo "\">
            <div class=\"col-lg-6 col-md-8 col-sm-12\">
              <div class=\"";
        // line 17
        if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_image_2", []))) {
            echo " feature-content ";
        } else {
            echo " app-content ";
        }
        echo "\">
                <div class=\"section-title mb-30\">
                  <span class=\"sub-title mb-15\">";
        // line 19
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_subtitle", [])), "html", null, true);
        echo "</span>
                  <h2 class=\"title-text mb-0\">
                    ";
        // line 21
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo "
                    ";
        // line 22
        if (($context["label"] ?? null)) {
            // line 23
            echo "                      ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null)), "html", null, true);
            echo "
                    ";
        }
        // line 25
        echo "                    ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "
                  </h2>
                </div>
                <div class=\"mb-45\">
                  ";
        // line 29
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "body", [])), "html", null, true);
        echo "
                  ";
        // line 30
        if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_features", []))) {
            // line 31
            echo "                    <div class=\"info-list ul-li-block  clearfix\">
                      ";
            // line 32
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_features", [])), "html", null, true);
            echo "
                    </div>
                  ";
        }
        // line 35
        echo "                </div>
                <div class=\"btns-group ul-li clearfix\">
                  <ul class=\"clearfix\">
                    ";
        // line 38
        if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_paragraphs", []))) {
            // line 39
            echo "                      ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_paragraphs", [])), "html", null, true);
            echo "
                    ";
        }
        // line 41
        echo "                    ";
        if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_link", []))) {
            // line 42
            echo "                      <li>
                        <a href=\"";
            // line 43
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link", []), 0, []), "#url", [], "array")), "html", null, true);
            echo "\" class=\"custom-btn\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link", []), 0, []), "#title", [], "array")), "html", null, true);
            echo "</a>
                      </li>
                    ";
        }
        // line 46
        echo "                    ";
        if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_link_2", []))) {
            // line 47
            echo "                      <li>
                        <a href=\"";
            // line 48
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link_2", []), 0, []), "#url", [], "array")), "html", null, true);
            echo "\" class=\"custom-btn-underline\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link_2", []), 0, []), "#title", [], "array")), "html", null, true);
            echo "</a>
                      </li>
                    ";
        }
        // line 51
        echo "                  </ul>
                </div>
              </div>
            </div>
            <div class=\"col-lg-5 col-md-5 col-sm-12\">
              <div
                class=\"";
        // line 57
        if (($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_image_2", [])) && twig_in_filter("1", $this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_style", []), 0, [], "array")))) {
            echo "feature-image-1 ";
        } elseif (($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_image_2", [])) && twig_in_filter("2", $this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_style", []), 0, [], "array")))) {
            echo " feature-image-6 float-left ";
        } elseif ((twig_test_empty($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_2", []), 0, [], "array")) && twig_in_filter("2", $this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_style", []), 0, [], "array")))) {
            echo " app-image-2 float-right ";
        } else {
            echo " app-image float-right ";
        }
        echo "\">
                ";
        // line 58
        if (twig_in_filter("2", $this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_style", []), 0, [], "array"))) {
            // line 59
            echo "                  <span class=\"bg-image\" data-aos=\"zoom-in\" data-aos-delay=\"100\">
                    <img src=\"";
            // line 60
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
            echo "/images/features/animation/6-img-1.png\" alt=\"image_not_found\">
                  </span>
                ";
        } else {
            // line 63
            echo "                  <span class=\"circle-image\">
                    <img src=\"";
            // line 64
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
            echo "/images/features/animation/1-img-1.png\" alt=\"image_not_found\">
                  </span>
                ";
        }
        // line 67
        echo "                ";
        if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_image", []))) {
            // line 68
            echo "                  <span class=\"phone-image\" data-aos=\"zoom-out\" data-aos-delay=\"200\">
                    <img src=\"";
            // line 69
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
            echo "\" alt=\"image_not_found\">
                  </span>
                ";
        }
        // line 72
        echo "                ";
        if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_image_2", []))) {
            // line 73
            echo "                  <span class=\"comment-image-1\" data-aos=\"fade-right\" data-aos-delay=\"400\">
                    <img src=\"";
            // line 74
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_2", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
            echo "\" alt=\"image_not_found\">
                  </span>
                ";
        }
        // line 77
        echo "                ";
        if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_image_3", []))) {
            // line 78
            echo "                  <span class=\"comment-image-2\" data-aos=\"fade-right\" data-aos-delay=\"600\">
                    <img src=\"";
            // line 79
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_3", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
            echo "\" alt=\"image_not_found\">
                  </span>
                ";
        }
        // line 82
        echo "                ";
        if (twig_in_filter("2", $this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_style", []), 0, [], "array"))) {
            // line 83
            echo "                  <span class=\"shape-image-1\" data-aos=\"fade-left\" data-aos-delay=\"600\">
                    <img src=\"";
            // line 84
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
            echo "/images/features/animation/6-img-2.png\" alt=\"image_not_found\">
                  </span>
                  <span class=\"shape-image-2\" data-aos=\"fade-right\" data-aos-delay=\"650\">
                    <img src=\"";
            // line 87
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
            echo "/images/features/animation/6-img-3.png\" alt=\"image_not_found\">
                  </span>
                  <span class=\"shape-image-3\" data-aos=\"fade-left\" data-aos-delay=\"700\">
                    <img src=\"";
            // line 90
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
            echo "/images/features/animation/6-img-4.png\" alt=\"image_not_found\">
                  </span>
                ";
        }
        // line 93
        echo "              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  ";
    }

    public function getTemplateName()
    {
        return "themes/appal/templates/blocks/cocoon/block--fullwidth-feature.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  285 => 93,  279 => 90,  273 => 87,  267 => 84,  264 => 83,  261 => 82,  255 => 79,  252 => 78,  249 => 77,  243 => 74,  240 => 73,  237 => 72,  231 => 69,  228 => 68,  225 => 67,  219 => 64,  216 => 63,  210 => 60,  207 => 59,  205 => 58,  193 => 57,  185 => 51,  177 => 48,  174 => 47,  171 => 46,  163 => 43,  160 => 42,  157 => 41,  151 => 39,  149 => 38,  144 => 35,  138 => 32,  135 => 31,  133 => 30,  129 => 29,  121 => 25,  115 => 23,  113 => 22,  109 => 21,  104 => 19,  95 => 17,  88 => 15,  77 => 12,  74 => 11,  69 => 100,  67 => 11,  62 => 10,  60 => 7,  59 => 6,  58 => 5,  57 => 4,  56 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{%
  set classes = [
    'block',
    'block-' ~ configuration.provider|clean_class,
    'block-' ~ plugin_id|clean_class,
    label ? 'has-title',
    content['body']['#bundle'] ? 'bundle-' ~ content['body']['#bundle'],
  ]
%}
<div{{ attributes.addClass(classes) }}>
  {% block content %}
    <section id=\"features-section\" class=\"{% if content.field_image_2 | render %} features-section  {% else %} app-section  {% endif %} sec-ptb-160 clearfix ccn-fullwidth-feature\">
      <div class=\"container\">
        <div class=\"feature-item\">
          <div class=\"row justify-content-lg-between justify-content-md-center {% if '1' in content.field_text_align[0] %} ccn-row-reverse {% endif %}\">
            <div class=\"col-lg-6 col-md-8 col-sm-12\">
              <div class=\"{% if content.field_image_2 | render %} feature-content {% else %} app-content {% endif %}\">
                <div class=\"section-title mb-30\">
                  <span class=\"sub-title mb-15\">{{ content.field_subtitle }}</span>
                  <h2 class=\"title-text mb-0\">
                    {{ title_prefix }}
                    {% if label %}
                      {{ label }}
                    {% endif %}
                    {{ title_suffix }}
                  </h2>
                </div>
                <div class=\"mb-45\">
                  {{ content.body }}
                  {% if content.field_features | render %}
                    <div class=\"info-list ul-li-block  clearfix\">
                      {{ content.field_features }}
                    </div>
                  {% endif %}
                </div>
                <div class=\"btns-group ul-li clearfix\">
                  <ul class=\"clearfix\">
                    {% if content.field_paragraphs | render %}
                      {{ content.field_paragraphs }}
                    {% endif %}
                    {% if content.field_link | render %}
                      <li>
                        <a href=\"{{ content.field_link.0['#url'] }}\" class=\"custom-btn\">{{ content.field_link.0['#title'] }}</a>
                      </li>
                    {% endif %}
                    {% if content.field_link_2 | render %}
                      <li>
                        <a href=\"{{ content.field_link_2.0['#url'] }}\" class=\"custom-btn-underline\">{{ content.field_link_2.0['#title'] }}</a>
                      </li>
                    {% endif %}
                  </ul>
                </div>
              </div>
            </div>
            <div class=\"col-lg-5 col-md-5 col-sm-12\">
              <div
                class=\"{% if (content.field_image_2|render) and ('1' in content.field_style[0]) %}feature-image-1 {% elseif (content.field_image_2|render) and ('2' in content.field_style[0]) %} feature-image-6 float-left {% elseif (content.field_image_2[0] is empty) and ('2' in content.field_style[0]) %} app-image-2 float-right {% else %} app-image float-right {% endif %}\">
                {% if '2' in content.field_style[0] %}
                  <span class=\"bg-image\" data-aos=\"zoom-in\" data-aos-delay=\"100\">
                    <img src=\"{{ base_path ~ directory }}/images/features/animation/6-img-1.png\" alt=\"image_not_found\">
                  </span>
                {% else %}
                  <span class=\"circle-image\">
                    <img src=\"{{ base_path ~ directory }}/images/features/animation/1-img-1.png\" alt=\"image_not_found\">
                  </span>
                {% endif %}
                {% if content.field_image | render %}
                  <span class=\"phone-image\" data-aos=\"zoom-out\" data-aos-delay=\"200\">
                    <img src=\"{{ file_url(content.field_image[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
                  </span>
                {% endif %}
                {% if content.field_image_2 | render %}
                  <span class=\"comment-image-1\" data-aos=\"fade-right\" data-aos-delay=\"400\">
                    <img src=\"{{ file_url(content.field_image_2[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
                  </span>
                {% endif %}
                {% if content.field_image_3 | render %}
                  <span class=\"comment-image-2\" data-aos=\"fade-right\" data-aos-delay=\"600\">
                    <img src=\"{{ file_url(content.field_image_3[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
                  </span>
                {% endif %}
                {% if '2' in content.field_style[0] %}
                  <span class=\"shape-image-1\" data-aos=\"fade-left\" data-aos-delay=\"600\">
                    <img src=\"{{ base_path ~ directory }}/images/features/animation/6-img-2.png\" alt=\"image_not_found\">
                  </span>
                  <span class=\"shape-image-2\" data-aos=\"fade-right\" data-aos-delay=\"650\">
                    <img src=\"{{ base_path ~ directory }}/images/features/animation/6-img-3.png\" alt=\"image_not_found\">
                  </span>
                  <span class=\"shape-image-3\" data-aos=\"fade-left\" data-aos-delay=\"700\">
                    <img src=\"{{ base_path ~ directory }}/images/features/animation/6-img-4.png\" alt=\"image_not_found\">
                  </span>
                {% endif %}
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  {% endblock %}
</div>
", "themes/appal/templates/blocks/cocoon/block--fullwidth-feature.html.twig", "/home/princewill/Projects/Drupal Projects/drupal/themes/appal/templates/blocks/cocoon/block--fullwidth-feature.html.twig");
    }
}
