<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/appal/templates/blocks/cocoon/block--fullwidth-feature-saas.html.twig */
class __TwigTemplate_dba8e752e57032b0f55a685579be380f91837fce17b3a1963119a20f320e3616 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 2, "block" => 11, "if" => 15];
        $filters = ["clean_class" => 4, "escape" => 10, "render" => 30];
        $functions = ["file_url" => 58];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'block', 'if'],
                ['clean_class', 'escape', 'render'],
                ['file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        $context["classes"] = [0 => "block", 1 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed($this->getAttribute(        // line 4
($context["configuration"] ?? null), "provider", [])))), 2 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 5
($context["plugin_id"] ?? null)))), 3 => ((        // line 6
($context["label"] ?? null)) ? ("has-title") : ("")), 4 => (($this->getAttribute($this->getAttribute(        // line 7
($context["content"] ?? null), "body", [], "array"), "#bundle", [], "array")) ? (("bundle-" . $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["content"] ?? null), "body", [], "array"), "#bundle", [], "array")))) : (""))];
        // line 10
        echo "<div";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method"), "addClass", [0 => "ccn--identify--fullwidth-feature-saas"], "method")), "html", null, true);
        echo ">
  ";
        // line 11
        $this->displayBlock('content', $context, $blocks);
        // line 73
        echo "</div>
";
    }

    // line 11
    public function block_content($context, array $blocks = [])
    {
        // line 12
        echo "    <section id=\"features-section\" class=\"features-section sec-ptb-160 clearfix ccn-fullwidth-feature\">
      <div class=\"container\">
        <div class=\"feature-item\">
          <div class=\"row justify-content-lg-between justify-content-md-center ";
        // line 15
        if (twig_in_filter("1", $this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_text_align", []), 0, [], "array"))) {
            echo " ccn-row-reverse ";
        }
        echo "\">
            <div class=\"col-lg-6 col-md-8 col-sm-12\">
              <div class=\"feature-content p-0\">
                <div class=\"section-title mb-60\">
                  <span class=\"sub-title mb-15\">";
        // line 19
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_supertitle", [])), "html", null, true);
        echo "</span>
                  <h2 class=\"title-text mb-0\">
                    ";
        // line 21
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo "
                    ";
        // line 22
        if (($context["label"] ?? null)) {
            // line 23
            echo "                      ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null)), "html", null, true);
            echo "
                    ";
        }
        // line 25
        echo "                    ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "
                  </h2>
                </div>
                <div class=\"mb-45\">
                  ";
        // line 29
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "body", [])), "html", null, true);
        echo "
                  ";
        // line 30
        if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_features", []))) {
            // line 31
            echo "                    <div class=\"info-list ul-li-block  clearfix\">
                      ";
            // line 32
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_features", [])), "html", null, true);
            echo "
                    </div>
                  ";
        }
        // line 35
        echo "                </div>
                <div class=\"btns-group ul-li clearfix\">
                  <ul class=\"clearfix\">
                    ";
        // line 38
        if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_paragraphs", []))) {
            // line 39
            echo "                      ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_paragraphs", [])), "html", null, true);
            echo "
                    ";
        }
        // line 41
        echo "                    ";
        if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_link", []))) {
            // line 42
            echo "                      <li>
                        <a href=\"";
            // line 43
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link", []), 0, []), "#url", [], "array")), "html", null, true);
            echo "\" class=\"custom-btn\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link", []), 0, []), "#title", [], "array")), "html", null, true);
            echo "</a>
                      </li>
                    ";
        }
        // line 46
        echo "                    ";
        if ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["content"] ?? null), "field_link_2", []))) {
            // line 47
            echo "                      <li>
                        <a href=\"";
            // line 48
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link_2", []), 0, []), "#url", [], "array")), "html", null, true);
            echo "\" class=\"custom-btn-underline\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link_2", []), 0, []), "#title", [], "array")), "html", null, true);
            echo "</a>
                      </li>
                    ";
        }
        // line 51
        echo "                  </ul>
                </div>
              </div>
            </div>
            <div class=\"col-lg-5 col-md-10 col-sm-12\">
              <div class=\"feature-image-8\">
                <span class=\"image-1\" data-aos=\"zoom-in\" data-aos-delay=\"200\">
                  <img src=\"";
        // line 58
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
                </span>
                <span class=\"image-2\" data-aos=\"zoom-out\" data-aos-delay=\"300\">
                  <img src=\"";
        // line 61
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_2", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
                </span>
                <span class=\"image-3\" data-aos=\"fade-up\" data-aos-delay=\"500\">
                  <img src=\"";
        // line 64
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_3", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  ";
    }

    public function getTemplateName()
    {
        return "themes/appal/templates/blocks/cocoon/block--fullwidth-feature-saas.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  193 => 64,  187 => 61,  181 => 58,  172 => 51,  164 => 48,  161 => 47,  158 => 46,  150 => 43,  147 => 42,  144 => 41,  138 => 39,  136 => 38,  131 => 35,  125 => 32,  122 => 31,  120 => 30,  116 => 29,  108 => 25,  102 => 23,  100 => 22,  96 => 21,  91 => 19,  82 => 15,  77 => 12,  74 => 11,  69 => 73,  67 => 11,  62 => 10,  60 => 7,  59 => 6,  58 => 5,  57 => 4,  56 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{%
  set classes = [
    'block',
    'block-' ~ configuration.provider|clean_class,
    'block-' ~ plugin_id|clean_class,
    label ? 'has-title',
    content['body']['#bundle'] ? 'bundle-' ~ content['body']['#bundle'],
  ]
%}
<div{{ attributes.addClass(classes).addClass('ccn--identify--fullwidth-feature-saas') }}>
  {% block content %}
    <section id=\"features-section\" class=\"features-section sec-ptb-160 clearfix ccn-fullwidth-feature\">
      <div class=\"container\">
        <div class=\"feature-item\">
          <div class=\"row justify-content-lg-between justify-content-md-center {% if '1' in content.field_text_align[0] %} ccn-row-reverse {% endif %}\">
            <div class=\"col-lg-6 col-md-8 col-sm-12\">
              <div class=\"feature-content p-0\">
                <div class=\"section-title mb-60\">
                  <span class=\"sub-title mb-15\">{{ content.field_supertitle }}</span>
                  <h2 class=\"title-text mb-0\">
                    {{ title_prefix }}
                    {% if label %}
                      {{ label }}
                    {% endif %}
                    {{ title_suffix }}
                  </h2>
                </div>
                <div class=\"mb-45\">
                  {{ content.body }}
                  {% if content.field_features | render %}
                    <div class=\"info-list ul-li-block  clearfix\">
                      {{ content.field_features }}
                    </div>
                  {% endif %}
                </div>
                <div class=\"btns-group ul-li clearfix\">
                  <ul class=\"clearfix\">
                    {% if content.field_paragraphs | render %}
                      {{ content.field_paragraphs }}
                    {% endif %}
                    {% if content.field_link | render %}
                      <li>
                        <a href=\"{{ content.field_link.0['#url'] }}\" class=\"custom-btn\">{{ content.field_link.0['#title'] }}</a>
                      </li>
                    {% endif %}
                    {% if content.field_link_2 | render %}
                      <li>
                        <a href=\"{{ content.field_link_2.0['#url'] }}\" class=\"custom-btn-underline\">{{ content.field_link_2.0['#title'] }}</a>
                      </li>
                    {% endif %}
                  </ul>
                </div>
              </div>
            </div>
            <div class=\"col-lg-5 col-md-10 col-sm-12\">
              <div class=\"feature-image-8\">
                <span class=\"image-1\" data-aos=\"zoom-in\" data-aos-delay=\"200\">
                  <img src=\"{{ file_url(content.field_image[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
                </span>
                <span class=\"image-2\" data-aos=\"zoom-out\" data-aos-delay=\"300\">
                  <img src=\"{{ file_url(content.field_image_2[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
                </span>
                <span class=\"image-3\" data-aos=\"fade-up\" data-aos-delay=\"500\">
                  <img src=\"{{ file_url(content.field_image_3[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  {% endblock %}
</div>
", "themes/appal/templates/blocks/cocoon/block--fullwidth-feature-saas.html.twig", "/home/princewill/Projects/Drupal Projects/drupal/themes/appal/templates/blocks/cocoon/block--fullwidth-feature-saas.html.twig");
    }
}
