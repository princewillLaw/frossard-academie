<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/appal/templates/page.html.twig */
class __TwigTemplate_ffaef3ae80baa6534332b4d26dba435907138256fe85d33c8e61d025c255774d extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 4];
        $filters = ["escape" => 35];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<header
  id=\"header-section\"
  class=\"header-section sticky-header clearfix ccn--lb-tagged
  ";
        // line 4
        if (($this->getAttribute(($context["node"] ?? null), "gettype", []) == "layout_builder_page")) {
            // line 5
            echo "    ";
            if (($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_header_color", []), "value", []) == 0)) {
                // line 6
                echo "      black-content
    ";
            } elseif (($this->getAttribute($this->getAttribute(            // line 7
($context["node"] ?? null), "field_header_color", []), "value", []) == 1)) {
                // line 8
                echo "      ccn-header-dark white-content ccn-logo-white
    ";
            } elseif (($this->getAttribute($this->getAttribute(            // line 9
($context["node"] ?? null), "field_header_color", []), "value", []) == 2)) {
                // line 10
                echo "      ccn-header-blue white-content ccn-logo-white
    ";
            } elseif (($this->getAttribute($this->getAttribute(            // line 11
($context["node"] ?? null), "field_header_color", []), "value", []) == 3)) {
                // line 12
                echo "      ccn-header-pink white-content ccn-logo-white
    ";
            } elseif (($this->getAttribute($this->getAttribute(            // line 13
($context["node"] ?? null), "field_header_color", []), "value", []) == 4)) {
                // line 14
                echo "      white-content ccn-logo-white
    ";
            }
            // line 16
            echo "  ";
        } else {
            // line 17
            echo "    white-content ccn-logo-white
  ";
        }
        // line 19
        echo "\">
  <div class=\"
  ";
        // line 21
        if (($this->getAttribute(($context["node"] ?? null), "gettype", []) == "layout_builder_page")) {
            // line 22
            echo "    ";
            if (($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_header_width", []), "value", []) == 1)) {
                // line 23
                echo "      container-fluid
    ";
            } else {
                // line 25
                echo "      container
    ";
            }
            // line 27
            echo "  ";
        } else {
            // line 28
            echo "    container-fluid
  ";
        }
        // line 30
        echo "  \">
    <div class=\"row\">
      <!-- brand-logo - satrt -->
      <div class=\"col-lg-2 col-md-6 col-sm-6\">
        <div class=\"brand-logo float-left\">
          <a href=\"";
        // line 35
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)), "html", null, true);
        echo "\" class=\"brand-link\">
            <img src=\"";
        // line 36
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["logo_path"] ?? null)), "html", null, true);
        echo "\" alt=\"logo_not_found\">
          </a>
        </div>
        <div class=\"brand-logo-white float-left\">
          <a href=\"";
        // line 40
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)), "html", null, true);
        echo "\" class=\"brand-link\">
            <img src=\"";
        // line 41
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/logo-white.png\" alt=\"logo_not_found\">
          </a>
        </div>
      </div>
      <!-- brand-logo - end -->
      <!-- main-menubar - satrt -->
      <div
        class=\"
      ";
        // line 49
        if (($this->getAttribute(($context["node"] ?? null), "gettype", []) == "layout_builder_page")) {
            // line 50
            echo "        ";
            if (($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_header_width", []), "value", []) == 1)) {
                // line 51
                echo "          col-lg-7
        ";
            } else {
                // line 53
                echo "          col-lg-8
        ";
            }
            // line 55
            echo "      ";
        } else {
            // line 56
            echo "        col-lg-7
      ";
        }
        // line 58
        echo "      \">
        <div class=\"main-menubar ul-li-right clearfix\">
          ";
        // line 60
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "main_menu", [])), "html", null, true);
        echo "
        </div>
      </div>
      <!-- main-menubar - end -->
      <!-- btns-group - start -->
      <div
        class=\"
      ";
        // line 67
        if (($this->getAttribute(($context["node"] ?? null), "gettype", []) == "layout_builder_page")) {
            // line 68
            echo "        ";
            if (($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_header_width", []), "value", []) == 1)) {
                // line 69
                echo "          col-lg-3
        ";
            } else {
                // line 71
                echo "          col-lg-2
        ";
            }
            // line 73
            echo "      ";
        } else {
            // line 74
            echo "        col-lg-3
      ";
        }
        // line 76
        echo "       col-md-6 col-sm-6\">
        <div class=\"btns-group ul-li-right clearfix\">
          <ul class=\"clearfix\">
            <li>
              <a href=\"";
        // line 80
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["header_cta_link"] ?? null)), "html", null, true);
        echo "\" class=\"custom-btn\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["header_cta_title"] ?? null)), "html", null, true);
        echo "</a>
            </li>
            ";
        // line 82
        if (($this->getAttribute(($context["node"] ?? null), "gettype", []) == "layout_builder_page")) {
            // line 83
            echo "              ";
            if (($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_side_navigation", []), "value", []) == 1)) {
                // line 84
                echo "                <li>
                  <button type=\"button\" id=\"sidebar-collapse\" class=\"mobile-menu-btn\">
                    <i class=\"flaticon-right-alignment\"></i>
                  </button>
                </li>
              ";
            } else {
                // line 90
                echo "                <li class=\"d-none\">
                  <button type=\"button\" id=\"sidebar-collapse\" class=\"mobile-menu-btn\">
                    <i class=\"flaticon-right-alignment\"></i>
                  </button>
                </li>
              ";
            }
            // line 96
            echo "            ";
        } else {
            // line 97
            echo "              <li>
                <button type=\"button\" id=\"sidebar-collapse\" class=\"mobile-menu-btn\">
                  <i class=\"flaticon-right-alignment\"></i>
                </button>
              </li>
            ";
        }
        // line 103
        echo "          </ul>
        </div>
      </div>
      <!-- btns-group - end -->
    </div>
  </div>
</header>
<div class=\"sidebar-menu-wrapper\">
  <div id=\"sidebar-menu\" class=\"sidebar-menu\">
    <div class=\"dismiss text-right mb-60 clearfix\">
      <span class=\"close-btn\">
        <i class=\"flaticon-cancel-music\"></i>
      </span>
    </div>
    ";
        // line 117
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "side_menu", [])), "html", null, true);
        echo "

    <div class=\"contact-info ul-li-block mb-60 clearfix\">
      <h2 class=\"sidebar-title mb-30\">Contact
        ";
        // line 121
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["site_name"] ?? null)), "html", null, true);
        echo "</h2>
      <ul class=\"clearfix\">
        ";
        // line 123
        if (($context["contact_address"] ?? null)) {
            // line 124
            echo "          <li>
            <span class=\"icon\">
              <i class='uil uil-map-marker'></i>
            </span>
            ";
            // line 128
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["contact_address"] ?? null)), "html", null, true);
            echo "
          </li>
        ";
        }
        // line 131
        echo "        ";
        if (($context["contact_website"] ?? null)) {
            // line 132
            echo "          <li>
            <span class=\"icon\">
              <i class='uil uil-envelope-alt'></i>
            </span>
            ";
            // line 136
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["contact_website"] ?? null)), "html", null, true);
            echo "
          </li>
        ";
        }
        // line 139
        echo "        ";
        if (($context["contact_phone"] ?? null)) {
            // line 140
            echo "          <li>
            <span class=\"icon\">
              <i class='uil uil-phone'></i>
            </span>
            ";
            // line 144
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["contact_phone"] ?? null)), "html", null, true);
            echo "
          </li>
        ";
        }
        // line 147
        echo "      </ul>
    </div>
    <div class=\"social-links ul-li clearfix\">
      <h2 class=\"sidebar-title mb-30\">follow us</h2>
      <ul class=\"clearfix\">
        ";
        // line 152
        if (($context["social_facebook"] ?? null)) {
            // line 153
            echo "          <li>
            <a href=\"";
            // line 154
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["social_facebook"] ?? null)), "html", null, true);
            echo "\">
              <i class=\"fab fa-facebook-f\" aria-hidden=\"true\"></i>
            </a>
          </li>
        ";
        }
        // line 159
        echo "        ";
        if (($context["social_twitter"] ?? null)) {
            // line 160
            echo "          <li>
            <a href=\"";
            // line 161
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["social_twitter"] ?? null)), "html", null, true);
            echo "\">
              <i class=\"fab fa-twitter\" aria-hidden=\"true\"></i>
            </a>
          </li>
        ";
        }
        // line 166
        echo "        ";
        if (($context["social_pinterest"] ?? null)) {
            // line 167
            echo "          <li>
            <a href=\"";
            // line 168
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["social_pinterest"] ?? null)), "html", null, true);
            echo "\">
              <i class=\"fab fa-pinterest-p\" aria-hidden=\"true\"></i>
            </a>
          </li>
        ";
        }
        // line 173
        echo "        ";
        if (($context["social_behance"] ?? null)) {
            // line 174
            echo "          <li>
            <a href=\"";
            // line 175
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["social_behance"] ?? null)), "html", null, true);
            echo "\">
              <i class=\"fab fa-behance\" aria-hidden=\"true\"></i>
            </a>
          </li>
        ";
        }
        // line 180
        echo "      </ul>
    </div>
  </div>
  <div class=\"overlay\"></div>
</div>
";
        // line 185
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "page_title", [])), "html", null, true);
        echo "
";
        // line 186
        if ($this->getAttribute(($context["page"] ?? null), "sidebar", [])) {
            // line 187
            echo "  <section id=\"blog-section\" class=\"blog-section sec-ptb-160 clearfix\">
    <div class=\"container\">
      <div class=\"row justify-content-center\">
        <div class=\"col-lg-8 col-md-10 col-sm-12\">
        ";
        }
        // line 192
        echo "        ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "
        ";
        // line 193
        if ($this->getAttribute(($context["page"] ?? null), "sidebar", [])) {
            // line 194
            echo "        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-12\">
          <aside id=\"sidebar-section\" class=\"sidebar-section clearfix\">
            ";
            // line 197
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar", [])), "html", null, true);
            echo "
          </aside>
        </div>
      </div>
    </div>
  </section>
";
        }
        // line 204
        echo "<footer id=\"footer-section\" class=\"footer-section clearfix\" style=\"background-image: url(";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/footer-bg-1.png);\">
  ";
        // line 205
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer", [])), "html", null, true);
        echo "
  <div class=\"footer-content mb-100 clearfix\" data-aos=\"fade-up\" data-aos-duration=\"450\" data-aos-delay=\"200\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-lg-3 col-md-6\">
          <div class=\"about-content\">
            <div class=\"brand-logo mb-30\">
              <a href=\"";
        // line 212
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)), "html", null, true);
        echo "\" class=\"brand-link\">
                <img src=\"";
        // line 213
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["logo_path"] ?? null)), "html", null, true);
        echo "\" alt=\"logo_not_found\">
              </a>
            </div>
            <p class=\"mb-30\">
              
            </p>
            <div class=\"social-links ul-li clearfix\">
              <ul class=\"clearfix\">
                ";
        // line 221
        if (($context["social_facebook"] ?? null)) {
            // line 222
            echo "                  <li>
                    <a href=\"";
            // line 223
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["social_facebook"] ?? null)), "html", null, true);
            echo "\">
                      <i class=\"fab fa-facebook-f\" aria-hidden=\"true\"></i>
                    </a>
                  </li>
                ";
        }
        // line 228
        echo "                ";
        if (($context["social_twitter"] ?? null)) {
            // line 229
            echo "                  <li>
                    <a href=\"";
            // line 230
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["social_twitter"] ?? null)), "html", null, true);
            echo "\">
                      <i class=\"fab fa-twitter\" aria-hidden=\"true\"></i>
                    </a>
                  </li>
                ";
        }
        // line 235
        echo "                ";
        if (($context["social_pinterest"] ?? null)) {
            // line 236
            echo "                  <li>
                    <a href=\"";
            // line 237
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["social_pinterest"] ?? null)), "html", null, true);
            echo "\">
                      <i class=\"fab fa-pinterest-p\" aria-hidden=\"true\"></i>
                    </a>
                  </li>
                ";
        }
        // line 242
        echo "                ";
        if (($context["social_behance"] ?? null)) {
            // line 243
            echo "                  <li>
                    <a href=\"";
            // line 244
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["social_behance"] ?? null)), "html", null, true);
            echo "\">
                      <i class=\"fab fa-behance\" aria-hidden=\"true\"></i>
                    </a>
                  </li>
                ";
        }
        // line 249
        echo "              </ul>
            </div>
          </div>
        </div>
        <div class=\"col-lg-2 col-md-6\">
          ";
        // line 254
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_menu_1", [])), "html", null, true);
        echo "
        </div>
        <div class=\"col-lg-2 col-md-6\">
          ";
        // line 257
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_menu_2", [])), "html", null, true);
        echo "
        </div>
        <div class=\"col-lg-2 col-md-6\">
          ";
        // line 260
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_menu_3", [])), "html", null, true);
        echo "
        </div>
        <div class=\"col-lg-3 col-md-6\">
          <div class=\"contact-info ul-li-block clearfix\">
            <h3 class=\"item-title\">address</h3>
            <ul class=\"clearfix\">
              ";
        // line 266
        if (($context["contact_phone"] ?? null)) {
            // line 267
            echo "                <li>
                  <small class=\"icon\">
                    <i class='uil uil-phone-pause'></i>
                  </small>
                  <span class=\"info-text\">";
            // line 271
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["contact_phone"] ?? null)), "html", null, true);
            echo "</span>
                </li>
              ";
        }
        // line 274
        echo "              ";
        if (($context["contact_website"] ?? null)) {
            // line 275
            echo "                <li>
                  <small class=\"icon\">
                    <i class='uil uil-envelope-minus'></i>
                  </small>
                  <span class=\"info-text\">";
            // line 279
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["contact_website"] ?? null)), "html", null, true);
            echo "</span>
                </li>
              ";
        }
        // line 282
        echo "              ";
        if (($context["contact_address"] ?? null)) {
            // line 283
            echo "                <li>
                  <small class=\"icon\">
                    <i class='uil uil-location-point'></i>
                  </small>
                  <span class=\"info-text\">";
            // line 287
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["contact_address"] ?? null)), "html", null, true);
            echo "</span>
                </li>
              ";
        }
        // line 290
        echo "            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class=\"container\">
    <div class=\"footer-bottom clearfix\">
      <div class=\"row\">
        <div class=\"col-lg-6 col-md-6 col-sm-12\">
          <p class=\"copyright-text mb-0\">
            ";
        // line 301
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "disclaimer_left", [])), "html", null, true);
        echo "
          </p>
        </div>
        <div class=\"col-lg-6 col-md-6 col-sm-12\">
          <div class=\"useful-links ul-li-right clearfix\">
            ";
        // line 306
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "disclaimer_right", [])), "html", null, true);
        echo "
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
";
    }

    public function getTemplateName()
    {
        return "themes/appal/templates/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  606 => 306,  598 => 301,  585 => 290,  579 => 287,  573 => 283,  570 => 282,  564 => 279,  558 => 275,  555 => 274,  549 => 271,  543 => 267,  541 => 266,  532 => 260,  526 => 257,  520 => 254,  513 => 249,  505 => 244,  502 => 243,  499 => 242,  491 => 237,  488 => 236,  485 => 235,  477 => 230,  474 => 229,  471 => 228,  463 => 223,  460 => 222,  458 => 221,  447 => 213,  443 => 212,  433 => 205,  428 => 204,  418 => 197,  413 => 194,  411 => 193,  406 => 192,  399 => 187,  397 => 186,  393 => 185,  386 => 180,  378 => 175,  375 => 174,  372 => 173,  364 => 168,  361 => 167,  358 => 166,  350 => 161,  347 => 160,  344 => 159,  336 => 154,  333 => 153,  331 => 152,  324 => 147,  318 => 144,  312 => 140,  309 => 139,  303 => 136,  297 => 132,  294 => 131,  288 => 128,  282 => 124,  280 => 123,  275 => 121,  268 => 117,  252 => 103,  244 => 97,  241 => 96,  233 => 90,  225 => 84,  222 => 83,  220 => 82,  213 => 80,  207 => 76,  203 => 74,  200 => 73,  196 => 71,  192 => 69,  189 => 68,  187 => 67,  177 => 60,  173 => 58,  169 => 56,  166 => 55,  162 => 53,  158 => 51,  155 => 50,  153 => 49,  142 => 41,  138 => 40,  131 => 36,  127 => 35,  120 => 30,  116 => 28,  113 => 27,  109 => 25,  105 => 23,  102 => 22,  100 => 21,  96 => 19,  92 => 17,  89 => 16,  85 => 14,  83 => 13,  80 => 12,  78 => 11,  75 => 10,  73 => 9,  70 => 8,  68 => 7,  65 => 6,  62 => 5,  60 => 4,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<header
  id=\"header-section\"
  class=\"header-section sticky-header clearfix ccn--lb-tagged
  {% if node.gettype == 'layout_builder_page' %}
    {% if (node.field_header_color.value) == 0 %}
      black-content
    {% elseif (node.field_header_color.value) == 1 %}
      ccn-header-dark white-content ccn-logo-white
    {% elseif (node.field_header_color.value) == 2 %}
      ccn-header-blue white-content ccn-logo-white
    {% elseif (node.field_header_color.value) == 3 %}
      ccn-header-pink white-content ccn-logo-white
    {% elseif (node.field_header_color.value) == 4 %}
      white-content ccn-logo-white
    {% endif %}
  {% else %}
    white-content ccn-logo-white
  {% endif %}
\">
  <div class=\"
  {% if node.gettype == 'layout_builder_page' %}
    {% if node.field_header_width.value == 1 %}
      container-fluid
    {% else %}
      container
    {% endif %}
  {% else %}
    container-fluid
  {% endif %}
  \">
    <div class=\"row\">
      <!-- brand-logo - satrt -->
      <div class=\"col-lg-2 col-md-6 col-sm-6\">
        <div class=\"brand-logo float-left\">
          <a href=\"{{ base_path }}\" class=\"brand-link\">
            <img src=\"{{ logo_path }}\" alt=\"logo_not_found\">
          </a>
        </div>
        <div class=\"brand-logo-white float-left\">
          <a href=\"{{ base_path }}\" class=\"brand-link\">
            <img src=\"{{ base_path ~ directory }}/logo-white.png\" alt=\"logo_not_found\">
          </a>
        </div>
      </div>
      <!-- brand-logo - end -->
      <!-- main-menubar - satrt -->
      <div
        class=\"
      {% if node.gettype == 'layout_builder_page' %}
        {% if node.field_header_width.value == 1 %}
          col-lg-7
        {% else %}
          col-lg-8
        {% endif %}
      {% else %}
        col-lg-7
      {% endif %}
      \">
        <div class=\"main-menubar ul-li-right clearfix\">
          {{ page.main_menu }}
        </div>
      </div>
      <!-- main-menubar - end -->
      <!-- btns-group - start -->
      <div
        class=\"
      {% if node.gettype == 'layout_builder_page' %}
        {% if node.field_header_width.value == 1 %}
          col-lg-3
        {% else %}
          col-lg-2
        {% endif %}
      {% else %}
        col-lg-3
      {% endif %}
       col-md-6 col-sm-6\">
        <div class=\"btns-group ul-li-right clearfix\">
          <ul class=\"clearfix\">
            <li>
              <a href=\"{{ header_cta_link }}\" class=\"custom-btn\">{{ header_cta_title }}</a>
            </li>
            {% if node.gettype == 'layout_builder_page' %}
              {% if node.field_side_navigation.value == 1 %}
                <li>
                  <button type=\"button\" id=\"sidebar-collapse\" class=\"mobile-menu-btn\">
                    <i class=\"flaticon-right-alignment\"></i>
                  </button>
                </li>
              {% else %}
                <li class=\"d-none\">
                  <button type=\"button\" id=\"sidebar-collapse\" class=\"mobile-menu-btn\">
                    <i class=\"flaticon-right-alignment\"></i>
                  </button>
                </li>
              {% endif %}
            {% else %}
              <li>
                <button type=\"button\" id=\"sidebar-collapse\" class=\"mobile-menu-btn\">
                  <i class=\"flaticon-right-alignment\"></i>
                </button>
              </li>
            {% endif %}
          </ul>
        </div>
      </div>
      <!-- btns-group - end -->
    </div>
  </div>
</header>
<div class=\"sidebar-menu-wrapper\">
  <div id=\"sidebar-menu\" class=\"sidebar-menu\">
    <div class=\"dismiss text-right mb-60 clearfix\">
      <span class=\"close-btn\">
        <i class=\"flaticon-cancel-music\"></i>
      </span>
    </div>
    {{ page.side_menu }}

    <div class=\"contact-info ul-li-block mb-60 clearfix\">
      <h2 class=\"sidebar-title mb-30\">Contact
        {{ site_name }}</h2>
      <ul class=\"clearfix\">
        {% if contact_address %}
          <li>
            <span class=\"icon\">
              <i class='uil uil-map-marker'></i>
            </span>
            {{ contact_address }}
          </li>
        {% endif %}
        {% if contact_website %}
          <li>
            <span class=\"icon\">
              <i class='uil uil-envelope-alt'></i>
            </span>
            {{ contact_website }}
          </li>
        {% endif %}
        {% if contact_phone %}
          <li>
            <span class=\"icon\">
              <i class='uil uil-phone'></i>
            </span>
            {{ contact_phone }}
          </li>
        {% endif %}
      </ul>
    </div>
    <div class=\"social-links ul-li clearfix\">
      <h2 class=\"sidebar-title mb-30\">follow us</h2>
      <ul class=\"clearfix\">
        {% if social_facebook %}
          <li>
            <a href=\"{{ social_facebook }}\">
              <i class=\"fab fa-facebook-f\" aria-hidden=\"true\"></i>
            </a>
          </li>
        {% endif %}
        {% if social_twitter %}
          <li>
            <a href=\"{{ social_twitter }}\">
              <i class=\"fab fa-twitter\" aria-hidden=\"true\"></i>
            </a>
          </li>
        {% endif %}
        {% if social_pinterest %}
          <li>
            <a href=\"{{ social_pinterest }}\">
              <i class=\"fab fa-pinterest-p\" aria-hidden=\"true\"></i>
            </a>
          </li>
        {% endif %}
        {% if social_behance %}
          <li>
            <a href=\"{{ social_behance }}\">
              <i class=\"fab fa-behance\" aria-hidden=\"true\"></i>
            </a>
          </li>
        {% endif %}
      </ul>
    </div>
  </div>
  <div class=\"overlay\"></div>
</div>
{{ page.page_title }}
{% if page.sidebar %}
  <section id=\"blog-section\" class=\"blog-section sec-ptb-160 clearfix\">
    <div class=\"container\">
      <div class=\"row justify-content-center\">
        <div class=\"col-lg-8 col-md-10 col-sm-12\">
        {% endif %}
        {{ page.content }}
        {% if page.sidebar %}
        </div>
        <div class=\"col-lg-4 col-md-6 col-sm-12\">
          <aside id=\"sidebar-section\" class=\"sidebar-section clearfix\">
            {{ page.sidebar }}
          </aside>
        </div>
      </div>
    </div>
  </section>
{% endif %}
<footer id=\"footer-section\" class=\"footer-section clearfix\" style=\"background-image: url({{ base_path ~ directory }}/images/footer-bg-1.png);\">
  {{ page.footer }}
  <div class=\"footer-content mb-100 clearfix\" data-aos=\"fade-up\" data-aos-duration=\"450\" data-aos-delay=\"200\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-lg-3 col-md-6\">
          <div class=\"about-content\">
            <div class=\"brand-logo mb-30\">
              <a href=\"{{ base_path }}\" class=\"brand-link\">
                <img src=\"{{ logo_path }}\" alt=\"logo_not_found\">
              </a>
            </div>
            <p class=\"mb-30\">
              
            </p>
            <div class=\"social-links ul-li clearfix\">
              <ul class=\"clearfix\">
                {% if social_facebook %}
                  <li>
                    <a href=\"{{ social_facebook }}\">
                      <i class=\"fab fa-facebook-f\" aria-hidden=\"true\"></i>
                    </a>
                  </li>
                {% endif %}
                {% if social_twitter %}
                  <li>
                    <a href=\"{{ social_twitter }}\">
                      <i class=\"fab fa-twitter\" aria-hidden=\"true\"></i>
                    </a>
                  </li>
                {% endif %}
                {% if social_pinterest %}
                  <li>
                    <a href=\"{{ social_pinterest }}\">
                      <i class=\"fab fa-pinterest-p\" aria-hidden=\"true\"></i>
                    </a>
                  </li>
                {% endif %}
                {% if social_behance %}
                  <li>
                    <a href=\"{{ social_behance }}\">
                      <i class=\"fab fa-behance\" aria-hidden=\"true\"></i>
                    </a>
                  </li>
                {% endif %}
              </ul>
            </div>
          </div>
        </div>
        <div class=\"col-lg-2 col-md-6\">
          {{ page.footer_menu_1 }}
        </div>
        <div class=\"col-lg-2 col-md-6\">
          {{ page.footer_menu_2 }}
        </div>
        <div class=\"col-lg-2 col-md-6\">
          {{ page.footer_menu_3 }}
        </div>
        <div class=\"col-lg-3 col-md-6\">
          <div class=\"contact-info ul-li-block clearfix\">
            <h3 class=\"item-title\">address</h3>
            <ul class=\"clearfix\">
              {% if contact_phone %}
                <li>
                  <small class=\"icon\">
                    <i class='uil uil-phone-pause'></i>
                  </small>
                  <span class=\"info-text\">{{ contact_phone }}</span>
                </li>
              {% endif %}
              {% if contact_website %}
                <li>
                  <small class=\"icon\">
                    <i class='uil uil-envelope-minus'></i>
                  </small>
                  <span class=\"info-text\">{{ contact_website}}</span>
                </li>
              {% endif %}
              {% if contact_address %}
                <li>
                  <small class=\"icon\">
                    <i class='uil uil-location-point'></i>
                  </small>
                  <span class=\"info-text\">{{ contact_address }}</span>
                </li>
              {% endif %}
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class=\"container\">
    <div class=\"footer-bottom clearfix\">
      <div class=\"row\">
        <div class=\"col-lg-6 col-md-6 col-sm-12\">
          <p class=\"copyright-text mb-0\">
            {{ page.disclaimer_left }}
          </p>
        </div>
        <div class=\"col-lg-6 col-md-6 col-sm-12\">
          <div class=\"useful-links ul-li-right clearfix\">
            {{ page.disclaimer_right }}
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
", "themes/appal/templates/page.html.twig", "/home/princewill/Projects/Drupal Projects/drupal/themes/appal/templates/page.html.twig");
    }
}
