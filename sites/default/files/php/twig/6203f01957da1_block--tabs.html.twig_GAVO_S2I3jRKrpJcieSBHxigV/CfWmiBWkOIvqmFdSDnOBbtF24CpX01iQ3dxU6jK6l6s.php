<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/appal/templates/blocks/cocoon/block--tabs.html.twig */
class __TwigTemplate_c8b795938212e23f0bcdbf176cad58ee75b31ef36f1d8ff833e32c0a56e0f115 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 2, "block" => 11, "if" => 19, "for" => 34];
        $filters = ["clean_class" => 4, "escape" => 10, "length" => 33];
        $functions = ["range" => 34];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'block', 'if', 'for'],
                ['clean_class', 'escape', 'length'],
                ['range']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        $context["classes"] = [0 => "block", 1 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed($this->getAttribute(        // line 4
($context["configuration"] ?? null), "provider", [])))), 2 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 5
($context["plugin_id"] ?? null)))), 3 => ((        // line 6
($context["label"] ?? null)) ? ("has-title") : ("")), 4 => (($this->getAttribute($this->getAttribute(        // line 7
($context["content"] ?? null), "body", [], "array"), "#bundle", [], "array")) ? (("bundle-" . $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["content"] ?? null), "body", [], "array"), "#bundle", [], "array")))) : (""))];
        // line 10
        echo "<div";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
        echo ">
  ";
        // line 11
        $this->displayBlock('content', $context, $blocks);
        // line 55
        echo "</div>
";
    }

    // line 11
    public function block_content($context, array $blocks = [])
    {
        // line 12
        echo "    <section id=\"extra-features-section\" class=\"extra-features-section sec-ptb-160 bg-light-gray clearfix\">
      <div class=\"container\">
        <div class=\"row justify-content-center\">
          <div class=\"col-lg-6 col-md-8 col-sm-12\">
            <div class=\"section-title text-center mb-100\">
              <h2 class=\"title-text mb-30\">
                ";
        // line 18
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo "
                ";
        // line 19
        if (($context["label"] ?? null)) {
            // line 20
            echo "                  ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null)), "html", null, true);
            echo "
                ";
        }
        // line 22
        echo "                ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "
              </h2>
              <p class=\"paragraph-text mb-0\">
                ";
        // line 25
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_subtitle", [])), "html", null, true);
        echo "
              </p>
            </div>
          </div>
        </div>
        <!-- features-nav - start -->
        <div class=\"features-nav ul-li-center mb-100\">
          <ul class=\"nav\" role=\"tablist\">
            ";
        // line 33
        $context["numTabsIndex"] = twig_length_filter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_paragraphs", []), "#items", [], "array")));
        // line 34
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(0, ($context["numTabsIndex"] ?? null)));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 35
            echo "              ";
            if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_paragraphs", []), $context["i"], [], "array"), "#paragraph", [], "array"), "id", [], "method")) {
                // line 36
                echo "                <li class=\"nav-item tab-";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_paragraphs", []), $context["i"], [], "array"), "#paragraph", [], "array"), "id", [], "method")), "html", null, true);
                echo "\">
                  <a class=\"nav-link\" data-toggle=\"tab\" href=\"#tab-";
                // line 37
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_paragraphs", []), $context["i"], [], "array"), "#paragraph", [], "array"), "id", [], "method")), "html", null, true);
                echo "\">
                    <span class=\"item-icon\"></span>
                    ";
                // line 39
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_paragraphs", []), $context["i"], [], "array"), "#paragraph", [], "array"), "field_title", []), "value", [])), "html", null, true);
                echo "
                  </a>
                </li>
              ";
            }
            // line 43
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "          </ul>
        </div>
        <!-- features-nav - end -->
        <!-- tab-content - start -->
        <div class=\"tab-content\">
          ";
        // line 49
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_paragraphs", [])), "html", null, true);
        echo "
        </div>
        <!-- tab-content - end -->
      </div>
    </section>
  ";
    }

    public function getTemplateName()
    {
        return "themes/appal/templates/blocks/cocoon/block--tabs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 49,  148 => 44,  142 => 43,  135 => 39,  130 => 37,  125 => 36,  122 => 35,  117 => 34,  115 => 33,  104 => 25,  97 => 22,  91 => 20,  89 => 19,  85 => 18,  77 => 12,  74 => 11,  69 => 55,  67 => 11,  62 => 10,  60 => 7,  59 => 6,  58 => 5,  57 => 4,  56 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{%
  set classes = [
    'block',
    'block-' ~ configuration.provider|clean_class,
    'block-' ~ plugin_id|clean_class,
    label ? 'has-title',
    content['body']['#bundle'] ? 'bundle-' ~ content['body']['#bundle'],
  ]
%}
<div{{ attributes.addClass(classes) }}>
  {% block content %}
    <section id=\"extra-features-section\" class=\"extra-features-section sec-ptb-160 bg-light-gray clearfix\">
      <div class=\"container\">
        <div class=\"row justify-content-center\">
          <div class=\"col-lg-6 col-md-8 col-sm-12\">
            <div class=\"section-title text-center mb-100\">
              <h2 class=\"title-text mb-30\">
                {{ title_prefix }}
                {% if label %}
                  {{ label }}
                {% endif %}
                {{ title_suffix }}
              </h2>
              <p class=\"paragraph-text mb-0\">
                {{ content.field_subtitle }}
              </p>
            </div>
          </div>
        </div>
        <!-- features-nav - start -->
        <div class=\"features-nav ul-li-center mb-100\">
          <ul class=\"nav\" role=\"tablist\">
            {% set numTabsIndex = content.field_paragraphs['#items'] | length %}
            {% for i in 0..numTabsIndex %}
              {% if content.field_paragraphs[i]['#paragraph'].id() %}
                <li class=\"nav-item tab-{{ content.field_paragraphs[i]['#paragraph'].id() }}\">
                  <a class=\"nav-link\" data-toggle=\"tab\" href=\"#tab-{{ content.field_paragraphs[i]['#paragraph'].id() }}\">
                    <span class=\"item-icon\"></span>
                    {{ content.field_paragraphs[i]['#paragraph'].field_title.value }}
                  </a>
                </li>
              {% endif %}
            {% endfor %}
          </ul>
        </div>
        <!-- features-nav - end -->
        <!-- tab-content - start -->
        <div class=\"tab-content\">
          {{ content.field_paragraphs }}
        </div>
        <!-- tab-content - end -->
      </div>
    </section>
  {% endblock %}
</div>
", "themes/appal/templates/blocks/cocoon/block--tabs.html.twig", "/home/princewill/Projects/Drupal Projects/drupal/themes/appal/templates/blocks/cocoon/block--tabs.html.twig");
    }
}
