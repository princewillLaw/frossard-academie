<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/appal/templates/fields/field.html.twig */
class __TwigTemplate_34a82551c9ad968be555233d34cad178cbea47c949cc017d5b5e89cb16467578 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 1, "for" => 4];
        $filters = ["escape" => 5];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if', 'for'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        if (($context["label_hidden"] ?? null)) {
            // line 2
            echo "  ";
            if (($context["multiple"] ?? null)) {
                // line 3
                echo "    ";
                // line 4
                echo "      ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 5
                    echo "        ";
                    echo " ";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "content", [])), "html", null, true);
                    echo " ";
                    // line 6
                    echo "      ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 7
                echo "    ";
                // line 8
                echo "  ";
            } else {
                // line 9
                echo "    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 10
                    echo "      ";
                    echo " ";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "content", [])), "html", null, true);
                    echo " ";
                    // line 11
                    echo "    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 12
                echo "  ";
            }
        } else {
            // line 14
            echo "  ";
            // line 16
            echo "    ";
            if (($context["multiple"] ?? null)) {
                // line 17
                echo "      ";
                // line 18
                echo "    ";
            }
            // line 19
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 20
                echo "      ";
                echo " ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "content", [])), "html", null, true);
                // line 21
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 22
            echo "    ";
            if (($context["multiple"] ?? null)) {
                // line 23
                echo "      ";
                // line 24
                echo "    ";
            }
            // line 25
            echo "  ";
        }
    }

    public function getTemplateName()
    {
        return "themes/appal/templates/fields/field.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 25,  133 => 24,  131 => 23,  128 => 22,  122 => 21,  118 => 20,  113 => 19,  110 => 18,  108 => 17,  105 => 16,  103 => 14,  99 => 12,  93 => 11,  88 => 10,  83 => 9,  80 => 8,  78 => 7,  72 => 6,  67 => 5,  62 => 4,  60 => 3,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% if label_hidden %}
  {% if multiple %}
    {#<div{{ attributes }}>#}
      {% for item in items %}
        {#<div{{ item.attributes }}>#} {{ item.content }} {# </div> #}
      {% endfor %}
    {#</div>#}
  {% else %}
    {% for item in items %}
      {#<div{{ attributes }}>#} {{ item.content }} {#</div>#}
    {% endfor %}
  {% endif %}
{% else %}
  {#<div{{ attributes }}>#}
{#    <div{{ title_attributes }}>{{ label }}</div>#}
    {% if multiple %}
      {#<div>#}
    {% endif %}
    {% for item in items %}
      {#<div{{ item.attributes }}>#} {{ item.content }}{# </div>#}
    {% endfor %}
    {% if multiple %}
      {#</div>#}
    {% endif %}
  {#</div>#}
{% endif %}", "themes/appal/templates/fields/field.html.twig", "/home/princewill/Projects/Drupal Projects/drupal/themes/appal/templates/fields/field.html.twig");
    }
}
