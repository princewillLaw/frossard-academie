<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/appal/templates/blocks/cocoon/block--fullwidth-feature-video.html.twig */
class __TwigTemplate_ae229ec3c22844b5539a65ccd43be62331d1579625907b5e0e5097ec6137b355 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 2, "block" => 11, "if" => 34];
        $filters = ["clean_class" => 4, "escape" => 10];
        $functions = ["file_url" => 21];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'block', 'if'],
                ['clean_class', 'escape'],
                ['file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        $context["classes"] = [0 => "block", 1 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed($this->getAttribute(        // line 4
($context["configuration"] ?? null), "provider", [])))), 2 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 5
($context["plugin_id"] ?? null)))), 3 => ((        // line 6
($context["label"] ?? null)) ? ("has-title") : ("")), 4 => (($this->getAttribute($this->getAttribute(        // line 7
($context["content"] ?? null), "body", [], "array"), "#bundle", [], "array")) ? (("bundle-" . $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["content"] ?? null), "body", [], "array"), "#bundle", [], "array")))) : (""))];
        // line 10
        echo "<div";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method"), "addClass", [0 => "ccn--identify--fullwidth-feature-video"], "method")), "html", null, true);
        echo ">
  ";
        // line 11
        $this->displayBlock('content', $context, $blocks);
        // line 62
        echo "</div>
";
    }

    // line 11
    public function block_content($context, array $blocks = [])
    {
        // line 12
        echo "  <section class=\"features-section sec-ptb-160 clearfix\">
  <div class=\"container\">

    <div class=\"feature-item\">
      <div class=\"row justify-content-lg-between justify-content-md-center\">

        <div class=\"col-lg-5 col-md-8 col-sm-12\">
          <div class=\"feature-image-2 text-center\">
            <span class=\"item-image\">
              <img src=\"";
        // line 21
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
            </span>
            <a class=\"popup-video\" href=\"";
        // line 23
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_youtube_video_embed", []), 0, [])), "html", null, true);
        echo "\" data-aos=\"zoom-in\" data-aos-delay=\"100\">
              <i class='uil uil-play'></i>
            </a>
          </div>
        </div>

        <div class=\"col-lg-6 col-md-8 col-sm-12\">
          <div class=\"feature-content p-0\">
            <h2 class=\"feature-item-title\">

              ";
        // line 33
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo "
              ";
        // line 34
        if (($context["label"] ?? null)) {
            // line 35
            echo "                ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null)), "html", null, true);
            echo "
              ";
        }
        // line 37
        echo "              ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "
            </h2>
            <p class=\"mb-0\">
              ";
        // line 40
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "body", [])), "html", null, true);
        echo "
            </p>

            <div class=\"service-list ul-li clearfix\">
              <ul class=\"clearfix\">
";
        // line 45
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_paragraphs", [])), "html", null, true);
        echo "
</ul>
            </div>

          </div>
        </div>

      </div>
    </div>

  </div>
</section>




  ";
    }

    public function getTemplateName()
    {
        return "themes/appal/templates/blocks/cocoon/block--fullwidth-feature-video.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 45,  125 => 40,  118 => 37,  112 => 35,  110 => 34,  106 => 33,  93 => 23,  88 => 21,  77 => 12,  74 => 11,  69 => 62,  67 => 11,  62 => 10,  60 => 7,  59 => 6,  58 => 5,  57 => 4,  56 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{%
  set classes = [
    'block',
    'block-' ~ configuration.provider|clean_class,
    'block-' ~ plugin_id|clean_class,
    label ? 'has-title',
    content['body']['#bundle'] ? 'bundle-' ~ content['body']['#bundle'],
  ]
%}
<div{{ attributes.addClass(classes).addClass('ccn--identify--fullwidth-feature-video') }}>
  {% block content %}
  <section class=\"features-section sec-ptb-160 clearfix\">
  <div class=\"container\">

    <div class=\"feature-item\">
      <div class=\"row justify-content-lg-between justify-content-md-center\">

        <div class=\"col-lg-5 col-md-8 col-sm-12\">
          <div class=\"feature-image-2 text-center\">
            <span class=\"item-image\">
              <img src=\"{{ file_url(content.field_image[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
            </span>
            <a class=\"popup-video\" href=\"{{ content.field_youtube_video_embed.0 }}\" data-aos=\"zoom-in\" data-aos-delay=\"100\">
              <i class='uil uil-play'></i>
            </a>
          </div>
        </div>

        <div class=\"col-lg-6 col-md-8 col-sm-12\">
          <div class=\"feature-content p-0\">
            <h2 class=\"feature-item-title\">

              {{ title_prefix }}
              {% if label %}
                {{ label }}
              {% endif %}
              {{ title_suffix }}
            </h2>
            <p class=\"mb-0\">
              {{ content.body }}
            </p>

            <div class=\"service-list ul-li clearfix\">
              <ul class=\"clearfix\">
{{ content.field_paragraphs }}
</ul>
            </div>

          </div>
        </div>

      </div>
    </div>

  </div>
</section>




  {% endblock %}
</div>
", "themes/appal/templates/blocks/cocoon/block--fullwidth-feature-video.html.twig", "/home/princewill/Projects/Drupal Projects/drupal/themes/appal/templates/blocks/cocoon/block--fullwidth-feature-video.html.twig");
    }
}
