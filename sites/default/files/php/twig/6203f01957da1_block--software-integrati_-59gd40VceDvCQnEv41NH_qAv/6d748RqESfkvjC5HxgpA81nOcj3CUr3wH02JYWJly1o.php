<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/appal/templates/blocks/cocoon/block--software-integrations.html.twig */
class __TwigTemplate_6c8e76c3d4e8e11ea065bed03286f187195dcd2278b1bfb1983e38f87858f037 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 2, "block" => 11, "if" => 14];
        $filters = ["clean_class" => 4, "escape" => 10];
        $functions = ["file_url" => 15];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'block', 'if'],
                ['clean_class', 'escape'],
                ['file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        $context["classes"] = [0 => "block", 1 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed($this->getAttribute(        // line 4
($context["configuration"] ?? null), "provider", [])))), 2 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 5
($context["plugin_id"] ?? null)))), 3 => ((        // line 6
($context["label"] ?? null)) ? ("has-title") : ("")), 4 => (($this->getAttribute($this->getAttribute(        // line 7
($context["content"] ?? null), "body", [], "array"), "#bundle", [], "array")) ? (("bundle-" . $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["content"] ?? null), "body", [], "array"), "#bundle", [], "array")))) : (""))];
        // line 10
        echo "<div";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
        echo ">
  ";
        // line 11
        $this->displayBlock('content', $context, $blocks);
        // line 57
        echo "</div>
";
    }

    // line 11
    public function block_content($context, array $blocks = [])
    {
        // line 12
        echo "    <section
      id=\"software-section\"
      class=\"software-section sec-ptb-160 clearfix ";
        // line 14
        if (twig_in_filter("2", $this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_style", []), 0, [], "array"))) {
            echo " ccn--software-dark ";
        } else {
            echo " ccn--software-light ";
        }
        echo "\"
      style=\"background-image: url(";
        // line 15
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo ");\">
      ";
        // line 16
        if (twig_in_filter("2", $this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_style", []), 0, [], "array"))) {
            // line 17
            echo "        ";
            // line 23
            echo "      ";
        } else {
            // line 24
            echo "        <span class=\"shape-img-1\" data-aos=\"fade-right\" data-aos-duration=\"1200\" data-aos-delay=\"300\">
          <img src=\"";
            // line 25
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
            echo "/images/software/shape-3.png\" alt=\"logo_not_found\">
        </span>
        <span class=\"shape-img-2\" data-aos=\"fade-left\" data-aos-duration=\"1200\" data-aos-delay=\"300\">
          <img src=\"";
            // line 28
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
            echo "/images/software/shape-4.png\" alt=\"logo_not_found\">
        </span>
      ";
        }
        // line 31
        echo "      <div class=\"container\">
        <div class=\"row justify-content-center\">
          <div class=\"col-lg-6 col-md-8 col-sm-12\">
            <div class=\"section-title mb-100 text-center\">
              <span class=\"sub-title mb-15\">";
        // line 35
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_supertitle", [])), "html", null, true);
        echo "</span>
              <h2 class=\"title-text mb-30\">
                ";
        // line 37
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo "
                ";
        // line 38
        if (($context["label"] ?? null)) {
            // line 39
            echo "                  ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null)), "html", null, true);
            echo "
                ";
        }
        // line 41
        echo "                ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "
              </h2>
              <p class=\"paragraph-text mb-0\">
                ";
        // line 44
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_subtitle", [])), "html", null, true);
        echo "
              </p>
            </div>
          </div>
          <div class=\"col-lg-8 col-md-12 col-sm-12\">
            <div class=\"software-container\">
              ";
        // line 50
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_paragraphs", [])), "html", null, true);
        echo "
            </div>
          </div>
        </div>
      </div>
    </section>
  ";
    }

    public function getTemplateName()
    {
        return "themes/appal/templates/blocks/cocoon/block--software-integrations.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  154 => 50,  145 => 44,  138 => 41,  132 => 39,  130 => 38,  126 => 37,  121 => 35,  115 => 31,  109 => 28,  103 => 25,  100 => 24,  97 => 23,  95 => 17,  93 => 16,  89 => 15,  81 => 14,  77 => 12,  74 => 11,  69 => 57,  67 => 11,  62 => 10,  60 => 7,  59 => 6,  58 => 5,  57 => 4,  56 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{%
  set classes = [
    'block',
    'block-' ~ configuration.provider|clean_class,
    'block-' ~ plugin_id|clean_class,
    label ? 'has-title',
    content['body']['#bundle'] ? 'bundle-' ~ content['body']['#bundle'],
  ]
%}
<div{{ attributes.addClass(classes) }}>
  {% block content %}
    <section
      id=\"software-section\"
      class=\"software-section sec-ptb-160 clearfix {% if '2' in content.field_style[0] %} ccn--software-dark {% else %} ccn--software-light {% endif %}\"
      style=\"background-image: url({{ file_url(content.field_image[0]['#media'].field_media_image.entity.uri.value) }});\">
      {% if '2' in content.field_style[0] %}
        {#<span class=\"shape-img-1\" data-aos=\"fade-right\" data-aos-duration=\"1200\" data-aos-delay=\"300\">
          <img src=\"{{ base_path ~ directory }}/images/software/shape-1.png\" alt=\"logo_not_found\">
        </span>
        <span class=\"shape-img-2\" data-aos=\"fade-left\" data-aos-duration=\"1200\" data-aos-delay=\"300\">
          <img src=\"{{ base_path ~ directory }}/images/software/shape-2.png\" alt=\"logo_not_found\">
        </span>#}
      {% else %}
        <span class=\"shape-img-1\" data-aos=\"fade-right\" data-aos-duration=\"1200\" data-aos-delay=\"300\">
          <img src=\"{{ base_path ~ directory }}/images/software/shape-3.png\" alt=\"logo_not_found\">
        </span>
        <span class=\"shape-img-2\" data-aos=\"fade-left\" data-aos-duration=\"1200\" data-aos-delay=\"300\">
          <img src=\"{{ base_path ~ directory }}/images/software/shape-4.png\" alt=\"logo_not_found\">
        </span>
      {% endif %}
      <div class=\"container\">
        <div class=\"row justify-content-center\">
          <div class=\"col-lg-6 col-md-8 col-sm-12\">
            <div class=\"section-title mb-100 text-center\">
              <span class=\"sub-title mb-15\">{{ content.field_supertitle }}</span>
              <h2 class=\"title-text mb-30\">
                {{ title_prefix }}
                {% if label %}
                  {{ label }}
                {% endif %}
                {{ title_suffix }}
              </h2>
              <p class=\"paragraph-text mb-0\">
                {{ content.field_subtitle }}
              </p>
            </div>
          </div>
          <div class=\"col-lg-8 col-md-12 col-sm-12\">
            <div class=\"software-container\">
              {{ content.field_paragraphs }}
            </div>
          </div>
        </div>
      </div>
    </section>
  {% endblock %}
</div>
", "themes/appal/templates/blocks/cocoon/block--software-integrations.html.twig", "/home/princewill/Projects/Drupal Projects/drupal/themes/appal/templates/blocks/cocoon/block--software-integrations.html.twig");
    }
}
