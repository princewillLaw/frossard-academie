<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/appal/templates/blocks/cocoon/hero-app/block--hero-app-creative.html.twig */
class __TwigTemplate_0c50e045d56702d4b5f5a74c62d1293128cc0a6f108c7be1b51e1d1f9cb5c8ec extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 2, "block" => 11, "if" => 27];
        $filters = ["clean_class" => 4, "escape" => 10];
        $functions = ["file_url" => 50];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'block', 'if'],
                ['clean_class', 'escape'],
                ['file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        $context["classes"] = [0 => "block", 1 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed($this->getAttribute(        // line 4
($context["configuration"] ?? null), "provider", [])))), 2 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 5
($context["plugin_id"] ?? null)))), 3 => ((        // line 6
($context["label"] ?? null)) ? ("has-title") : ("")), 4 => (($this->getAttribute($this->getAttribute(        // line 7
($context["content"] ?? null), "body", [], "array"), "#bundle", [], "array")) ? (("bundle-" . $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["content"] ?? null), "body", [], "array"), "#bundle", [], "array")))) : (""))];
        // line 10
        echo "<div";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
        echo ">
  ";
        // line 11
        $this->displayBlock('content', $context, $blocks);
        // line 101
        echo "</div>
";
    }

    // line 11
    public function block_content($context, array $blocks = [])
    {
        // line 12
        echo "  <section id=\"banner-section\" class=\"banner-section clearfix\">
  <div class=\"mobile-app-banner-1\">

    <div class=\"container\">
      <div class=\"row justify-content-lg-between justify-content-md-center\">

        <div class=\"col-lg-6 col-md-8 col-sm-12\">
          <!-- show on mobile device - start -->
          <div class=\"mobile-banner-image d-none\">
            <img src=\"";
        // line 21
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/mobile-banner/img-1.png\" alt=\"image_not_found\">
          </div>
          <!-- show on mobile device - end -->
          <div class=\"banner-content\">
            <h2>
              ";
        // line 26
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo "
              ";
        // line 27
        if (($context["label"] ?? null)) {
            // line 28
            echo "                ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null)), "html", null, true);
            echo "
              ";
        }
        // line 30
        echo "              ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "
            </h2>
              <p>";
        // line 32
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_body_plain", [])), "html", null, true);
        echo "</p>
            <div class=\"btns-group ul-li clearfix\">
              <ul class=\"clearfix\">
";
        // line 35
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_paragraphs", [])), "html", null, true);
        echo "
</ul>
            </div>
          </div>
        </div>

        <div class=\"col-lg-5 col-md-8 col-sm-12\">
          <div class=\"banner-image scene\">
            <span class=\"bg-image\" data-depth=\"0.2\">
              <small data-aos=\"zoom-in\" data-aos-duration=\"450\" data-aos-delay=\"100\">
                <img src=\"";
        // line 45
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/banner/mab-1-img-1.png\" alt=\"image_not_found\">
              </small>
            </span>
            <span class=\"phone-image\" data-depth=\"0.2\">
              <small data-aos=\"zoom-out\" data-aos-duration=\"500\" data-aos-delay=\"500\">
                <img src=\"";
        // line 50
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
              </small>
            </span>
            <span class=\"man-image-1\" data-depth=\"0.5\">
              <small data-aos=\"fade-right\" data-aos-duration=\"550\" data-aos-delay=\"800\">
                <img src=\"";
        // line 55
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_2", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
              </small>
            </span>
            <span class=\"man-image-2\" data-depth=\"0.5\">
              <small data-aos=\"fade-left\" data-aos-duration=\"600\" data-aos-delay=\"1000\">
                <img src=\"";
        // line 60
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_3", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
              </small>
            </span>
            <span class=\"man-image-3\" data-depth=\"0.3\">
              <small data-aos=\"fade-down\" data-aos-duration=\"650\" data-aos-delay=\"1200\">
                <img src=\"";
        // line 65
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_4", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
              </small>
            </span>
            <span class=\"cog-image\" data-depth=\"0.4\">
              <small data-aos=\"zoom-in\" data-aos-duration=\"1000\" data-aos-delay=\"1500\">
                <img src=\"";
        // line 70
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/banner/mab-1-img-6.png\" alt=\"image_not_found\">
              </small>
            </span>
            <span class=\"leaf-image-1\" data-depth=\"0.4\">
              <small data-aos=\"fade-left\" data-aos-duration=\"450\" data-aos-delay=\"1500\">
                <img src=\"";
        // line 75
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_5", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
              </small>
            </span>
            <span class=\"leaf-image-2\" data-depth=\"0.4\">
              <small data-aos=\"fade-right\" data-aos-duration=\"450\" data-aos-delay=\"1500\">
                <img src=\"";
        // line 80
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_6", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
              </small>
            </span>
          </div>
        </div>

      </div>
    </div>

    <small class=\"shape-1\"><img src=\"";
        // line 89
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/shapes/cross-2.png\" alt=\"cross_shape_image\"></small>
    <small class=\"shape-2\"><img src=\"";
        // line 90
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/shapes/flow-1-2.png\" alt=\"flow_shape_image\"></small>
    <small class=\"shape-3\"><img src=\"";
        // line 91
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/shapes/circle-half-2.png\" alt=\"circle_half_shape_image\"></small>
    <small class=\"shape-4\"><img src=\"";
        // line 92
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/shapes/circle-half-2.png\" alt=\"circle_half_shape_image\"></small>
    <small class=\"shape-5\"><img src=\"";
        // line 93
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/shapes/flow-2-2.png\" alt=\"flow_shape_image\"></small>
    <small class=\"shape-6\"><img src=\"";
        // line 94
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/shapes/box-2.png\" alt=\"box_shape_image\"></small>
    <small class=\"shape-7\"><img src=\"";
        // line 95
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/shapes/circle-2.png\" alt=\"circle_shape_image\"></small>

  </div>
</section>

  ";
    }

    public function getTemplateName()
    {
        return "themes/appal/templates/blocks/cocoon/hero-app/block--hero-app-creative.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  225 => 95,  221 => 94,  217 => 93,  213 => 92,  209 => 91,  205 => 90,  201 => 89,  189 => 80,  181 => 75,  173 => 70,  165 => 65,  157 => 60,  149 => 55,  141 => 50,  133 => 45,  120 => 35,  114 => 32,  108 => 30,  102 => 28,  100 => 27,  96 => 26,  88 => 21,  77 => 12,  74 => 11,  69 => 101,  67 => 11,  62 => 10,  60 => 7,  59 => 6,  58 => 5,  57 => 4,  56 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{%
  set classes = [
    'block',
    'block-' ~ configuration.provider|clean_class,
    'block-' ~ plugin_id|clean_class,
    label ? 'has-title',
    content['body']['#bundle'] ? 'bundle-' ~ content['body']['#bundle'],
  ]
%}
<div{{ attributes.addClass(classes) }}>
  {% block content %}
  <section id=\"banner-section\" class=\"banner-section clearfix\">
  <div class=\"mobile-app-banner-1\">

    <div class=\"container\">
      <div class=\"row justify-content-lg-between justify-content-md-center\">

        <div class=\"col-lg-6 col-md-8 col-sm-12\">
          <!-- show on mobile device - start -->
          <div class=\"mobile-banner-image d-none\">
            <img src=\"{{ base_path ~ directory }}/images/mobile-banner/img-1.png\" alt=\"image_not_found\">
          </div>
          <!-- show on mobile device - end -->
          <div class=\"banner-content\">
            <h2>
              {{ title_prefix }}
              {% if label %}
                {{ label }}
              {% endif %}
              {{ title_suffix }}
            </h2>
              <p>{{ content.field_body_plain }}</p>
            <div class=\"btns-group ul-li clearfix\">
              <ul class=\"clearfix\">
{{ content.field_paragraphs }}
</ul>
            </div>
          </div>
        </div>

        <div class=\"col-lg-5 col-md-8 col-sm-12\">
          <div class=\"banner-image scene\">
            <span class=\"bg-image\" data-depth=\"0.2\">
              <small data-aos=\"zoom-in\" data-aos-duration=\"450\" data-aos-delay=\"100\">
                <img src=\"{{ base_path ~ directory }}/images/banner/mab-1-img-1.png\" alt=\"image_not_found\">
              </small>
            </span>
            <span class=\"phone-image\" data-depth=\"0.2\">
              <small data-aos=\"zoom-out\" data-aos-duration=\"500\" data-aos-delay=\"500\">
                <img src=\"{{ file_url(content.field_image[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
              </small>
            </span>
            <span class=\"man-image-1\" data-depth=\"0.5\">
              <small data-aos=\"fade-right\" data-aos-duration=\"550\" data-aos-delay=\"800\">
                <img src=\"{{ file_url(content.field_image_2[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
              </small>
            </span>
            <span class=\"man-image-2\" data-depth=\"0.5\">
              <small data-aos=\"fade-left\" data-aos-duration=\"600\" data-aos-delay=\"1000\">
                <img src=\"{{ file_url(content.field_image_3[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
              </small>
            </span>
            <span class=\"man-image-3\" data-depth=\"0.3\">
              <small data-aos=\"fade-down\" data-aos-duration=\"650\" data-aos-delay=\"1200\">
                <img src=\"{{ file_url(content.field_image_4[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
              </small>
            </span>
            <span class=\"cog-image\" data-depth=\"0.4\">
              <small data-aos=\"zoom-in\" data-aos-duration=\"1000\" data-aos-delay=\"1500\">
                <img src=\"{{ base_path ~ directory }}/images/banner/mab-1-img-6.png\" alt=\"image_not_found\">
              </small>
            </span>
            <span class=\"leaf-image-1\" data-depth=\"0.4\">
              <small data-aos=\"fade-left\" data-aos-duration=\"450\" data-aos-delay=\"1500\">
                <img src=\"{{ file_url(content.field_image_5[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
              </small>
            </span>
            <span class=\"leaf-image-2\" data-depth=\"0.4\">
              <small data-aos=\"fade-right\" data-aos-duration=\"450\" data-aos-delay=\"1500\">
                <img src=\"{{ file_url(content.field_image_6[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
              </small>
            </span>
          </div>
        </div>

      </div>
    </div>

    <small class=\"shape-1\"><img src=\"{{ base_path ~ directory }}/images/shapes/cross-2.png\" alt=\"cross_shape_image\"></small>
    <small class=\"shape-2\"><img src=\"{{ base_path ~ directory }}/images/shapes/flow-1-2.png\" alt=\"flow_shape_image\"></small>
    <small class=\"shape-3\"><img src=\"{{ base_path ~ directory }}/images/shapes/circle-half-2.png\" alt=\"circle_half_shape_image\"></small>
    <small class=\"shape-4\"><img src=\"{{ base_path ~ directory }}/images/shapes/circle-half-2.png\" alt=\"circle_half_shape_image\"></small>
    <small class=\"shape-5\"><img src=\"{{ base_path ~ directory }}/images/shapes/flow-2-2.png\" alt=\"flow_shape_image\"></small>
    <small class=\"shape-6\"><img src=\"{{ base_path ~ directory }}/images/shapes/box-2.png\" alt=\"box_shape_image\"></small>
    <small class=\"shape-7\"><img src=\"{{ base_path ~ directory }}/images/shapes/circle-2.png\" alt=\"circle_shape_image\"></small>

  </div>
</section>

  {% endblock %}
</div>
", "themes/appal/templates/blocks/cocoon/hero-app/block--hero-app-creative.html.twig", "/home/princewill/Projects/Drupal Projects/drupal/themes/appal/templates/blocks/cocoon/hero-app/block--hero-app-creative.html.twig");
    }
}
