<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/appal/templates/blocks/cocoon/block--price-tables-tabbed.html.twig */
class __TwigTemplate_972d2afb4f1f94623a8cb9f8cb31d5385ca1b24da10c6e2d64633690dfc36720 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 2, "block" => 11, "if" => 20];
        $filters = ["clean_class" => 4, "escape" => 10];
        $functions = ["file_url" => 39];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'block', 'if'],
                ['clean_class', 'escape'],
                ['file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        $context["classes"] = [0 => "block", 1 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed($this->getAttribute(        // line 4
($context["configuration"] ?? null), "provider", [])))), 2 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 5
($context["plugin_id"] ?? null)))), 3 => ((        // line 6
($context["label"] ?? null)) ? ("has-title") : ("")), 4 => (($this->getAttribute($this->getAttribute(        // line 7
($context["content"] ?? null), "body", [], "array"), "#bundle", [], "array")) ? (("bundle-" . $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["content"] ?? null), "body", [], "array"), "#bundle", [], "array")))) : (""))];
        // line 10
        echo "<div";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method"), "addClass", [0 => "ccn--identify--price-tables-tabbed"], "method")), "html", null, true);
        echo ">
  ";
        // line 11
        $this->displayBlock('content', $context, $blocks);
        // line 62
        echo "</div>
";
    }

    // line 11
    public function block_content($context, array $blocks = [])
    {
        // line 12
        echo "    <section id=\"pricing-section\" class=\"pricing-section clearfix\">
      <div class=\"container\">
        <div class=\"row justify-content-center\">
          <div class=\"col-lg-6 col-md-8 col-sm-12\">
            <div class=\"section-title mb-100 text-center\">
              <span class=\"sub-title mb-15\">";
        // line 17
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_subtitle", [])), "html", null, true);
        echo "</span>
              <h2 class=\"title-text mb-0\">
                ";
        // line 19
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo "
                ";
        // line 20
        if (($context["label"] ?? null)) {
            // line 21
            echo "                  ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null)), "html", null, true);
            echo "
                ";
        }
        // line 23
        echo "                ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "
              </h2>
            </div>
          </div>
        </div>
      </div>
      <div class=\"pricing-tab-nav ul-li-center clearfix\">
        <ul class=\"nav\" role=\"tablist\">
          <li class=\"nav-item\">
            <a class=\"nav-link active\" data-toggle=\"tab\" href=\"#monthly\">monthly</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" data-toggle=\"tab\" href=\"#annually\">annually</a>
          </li>
        </ul>
      </div>
      <div class=\"bg-image sec-ptb-160 pt-0 clearfix\" style=\"background-image: url(";
        // line 39
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo ");\">
        <div class=\"tab-content\">
          <div id=\"monthly\" class=\"tab-pane active fade\">
            <div class=\"container\">
              <div class=\"row\">
                ";
        // line 44
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_paragraphs", [])), "html", null, true);
        echo "
              </div>
            </div>
          </div>
          <div id=\"annually\" class=\"tab-pane fade\">
            <div class=\"container\">
              <div class=\"row\">
                ";
        // line 51
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_paragraphs_2", [])), "html", null, true);
        echo "
              </div>
            </div>
          </div>
        </div>
        <div class=\"review-btn text-center\">
          <a href=\"";
        // line 57
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link", []), 0, []), "#url", [], "array")), "html", null, true);
        echo "\" class=\"custom-btn-underline\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link", []), 0, []), "#title", [], "array")), "html", null, true);
        echo "</a>
        </div>
      </div>
    </section>
  ";
    }

    public function getTemplateName()
    {
        return "themes/appal/templates/blocks/cocoon/block--price-tables-tabbed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  148 => 57,  139 => 51,  129 => 44,  121 => 39,  101 => 23,  95 => 21,  93 => 20,  89 => 19,  84 => 17,  77 => 12,  74 => 11,  69 => 62,  67 => 11,  62 => 10,  60 => 7,  59 => 6,  58 => 5,  57 => 4,  56 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{%
  set classes = [
    'block',
    'block-' ~ configuration.provider|clean_class,
    'block-' ~ plugin_id|clean_class,
    label ? 'has-title',
    content['body']['#bundle'] ? 'bundle-' ~ content['body']['#bundle'],
  ]
%}
<div{{ attributes.addClass(classes).addClass('ccn--identify--price-tables-tabbed') }}>
  {% block content %}
    <section id=\"pricing-section\" class=\"pricing-section clearfix\">
      <div class=\"container\">
        <div class=\"row justify-content-center\">
          <div class=\"col-lg-6 col-md-8 col-sm-12\">
            <div class=\"section-title mb-100 text-center\">
              <span class=\"sub-title mb-15\">{{ content.field_subtitle }}</span>
              <h2 class=\"title-text mb-0\">
                {{ title_prefix }}
                {% if label %}
                  {{ label }}
                {% endif %}
                {{ title_suffix }}
              </h2>
            </div>
          </div>
        </div>
      </div>
      <div class=\"pricing-tab-nav ul-li-center clearfix\">
        <ul class=\"nav\" role=\"tablist\">
          <li class=\"nav-item\">
            <a class=\"nav-link active\" data-toggle=\"tab\" href=\"#monthly\">monthly</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" data-toggle=\"tab\" href=\"#annually\">annually</a>
          </li>
        </ul>
      </div>
      <div class=\"bg-image sec-ptb-160 pt-0 clearfix\" style=\"background-image: url({{ file_url(content.field_image[0]['#media'].field_media_image.entity.uri.value) }});\">
        <div class=\"tab-content\">
          <div id=\"monthly\" class=\"tab-pane active fade\">
            <div class=\"container\">
              <div class=\"row\">
                {{ content.field_paragraphs }}
              </div>
            </div>
          </div>
          <div id=\"annually\" class=\"tab-pane fade\">
            <div class=\"container\">
              <div class=\"row\">
                {{ content.field_paragraphs_2 }}
              </div>
            </div>
          </div>
        </div>
        <div class=\"review-btn text-center\">
          <a href=\"{{ content.field_link.0['#url'] }}\" class=\"custom-btn-underline\">{{ content.field_link.0['#title'] }}</a>
        </div>
      </div>
    </section>
  {% endblock %}
</div>
", "themes/appal/templates/blocks/cocoon/block--price-tables-tabbed.html.twig", "/home/princewill/Projects/Drupal Projects/drupal/themes/appal/templates/blocks/cocoon/block--price-tables-tabbed.html.twig");
    }
}
