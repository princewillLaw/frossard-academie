<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/appal/templates/blocks/cocoon/hero-saas/block--hero-saas-modern.html.twig */
class __TwigTemplate_64458e12ae588d7a1fa3ae6f8df98a537b1f6e07bc717b0d60ea1ce7bc99fcc8 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 2, "block" => 11, "if" => 29];
        $filters = ["clean_class" => 4, "escape" => 10];
        $functions = ["file_url" => 52];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'block', 'if'],
                ['clean_class', 'escape'],
                ['file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        $context["classes"] = [0 => "block", 1 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed($this->getAttribute(        // line 4
($context["configuration"] ?? null), "provider", [])))), 2 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 5
($context["plugin_id"] ?? null)))), 3 => ((        // line 6
($context["label"] ?? null)) ? ("has-title") : ("")), 4 => (($this->getAttribute($this->getAttribute(        // line 7
($context["content"] ?? null), "body", [], "array"), "#bundle", [], "array")) ? (("bundle-" . $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["content"] ?? null), "body", [], "array"), "#bundle", [], "array")))) : (""))];
        // line 10
        echo "<div";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method"), "addClass", [0 => "ccn--identify--home-page-10"], "method")), "html", null, true);
        echo ">
  ";
        // line 11
        $this->displayBlock('content', $context, $blocks);
        // line 93
        echo "</div>
";
    }

    // line 11
    public function block_content($context, array $blocks = [])
    {
        // line 12
        echo "

  <section id=\"banner-section\" class=\"banner-section clearfix\">
    <div class=\"sass-banner-4 clearfix\">

      <div class=\"container\">
        <div class=\"row justify-content-lg-between justify-content-md-center\">

          <div class=\"col-lg-7 col-md-8 col-sm-12\">
            <!-- show on mobile device - start -->
            <div class=\"mobile-banner-image d-none\">
              <img src=\"";
        // line 23
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/mobile-banner/img-10.png\" alt=\"image_not_found\">
            </div>
            <!-- show on mobile device - end -->
            <div class=\"banner-content\">
              <h1 class=\"title-text mb-30\">
                ";
        // line 28
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo "
                ";
        // line 29
        if (($context["label"] ?? null)) {
            // line 30
            echo "                  ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null)), "html", null, true);
            echo "
                ";
        }
        // line 32
        echo "                ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "

              </h1>
              <p>
                ";
        // line 36
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_body_plain", [])), "html", null, true);
        echo "
              </p>
              <div class=\"btns-group ul-li clearfix\">
                <ul class=\"clearfix\">
                  <li><a href=\"";
        // line 40
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link", []), 0, []), "#url", [], "array")), "html", null, true);
        echo "\" class=\"custom-btn\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_link", []), 0, []), "#title", [], "array")), "html", null, true);
        echo "</a></li>
                  <li>
                    <p class=\"info-text mb-0\">";
        // line 42
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_subtitle", [])), "html", null, true);
        echo "</p>
                  </li>
                </ul>
              </div>
            </div>
          </div>

          <div class=\"col-lg-5\">
            <div class=\"banner-item-image\">
              <span class=\"laptop-image\" data-aos=\"fade-left\" data-aos-delay=\"100\">
                <img src=\"";
        // line 52
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
              </span>
              <span class=\"bg-image\">
                <img src=\"";
        // line 55
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/banner/shape-3.png\" alt=\"image_not_found\">
              </span>
              <span class=\"shape-image\" data-aos=\"zoom-in\" data-aos-delay=\"300\">
                <img src=\"";
        // line 58
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/banner/shape-4.png\" alt=\"image_not_found\">
              </span>
              <a class=\"popup-video\" href=\"";
        // line 60
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_youtube_video_embed", []), 0, [])), "html", null, true);
        echo "\" data-aos=\"zoom-in\" data-aos-delay=\"900\">
                <i class='uil uil-play'></i>
              </a>
            </div>
          </div>

        </div>
      </div>

      <span class=\"shape-1\">
        <img src=\"";
        // line 70
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/shapes/cross-2.png\" alt=\"image_not_found\">
      </span>
      <span class=\"shape-2\">
        <img src=\"";
        // line 73
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/shapes/flow-1-2.png\" alt=\"image_not_found\">
      </span>
      <span class=\"shape-3\">
        <img src=\"";
        // line 76
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/shapes/circle-half-2.png\" alt=\"image_not_found\">
      </span>
      <span class=\"shape-4\">
        <img src=\"";
        // line 79
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/shapes/circle-half-2.png\" alt=\"image_not_found\">
      </span>
      <span class=\"shape-5\">
        <img src=\"";
        // line 82
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/shapes/box-2.png\" alt=\"image_not_found\">
      </span>
      <span class=\"shape-6\">
        <img src=\"";
        // line 85
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/shapes/flow-2-2.png\" alt=\"image_not_found\">
      </span>

    </div>
  </section>


  ";
    }

    public function getTemplateName()
    {
        return "themes/appal/templates/blocks/cocoon/hero-saas/block--hero-saas-modern.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  205 => 85,  199 => 82,  193 => 79,  187 => 76,  181 => 73,  175 => 70,  162 => 60,  157 => 58,  151 => 55,  145 => 52,  132 => 42,  125 => 40,  118 => 36,  110 => 32,  104 => 30,  102 => 29,  98 => 28,  90 => 23,  77 => 12,  74 => 11,  69 => 93,  67 => 11,  62 => 10,  60 => 7,  59 => 6,  58 => 5,  57 => 4,  56 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{%
  set classes = [
    'block',
    'block-' ~ configuration.provider|clean_class,
    'block-' ~ plugin_id|clean_class,
    label ? 'has-title',
    content['body']['#bundle'] ? 'bundle-' ~ content['body']['#bundle'],
  ]
%}
<div{{ attributes.addClass(classes).addClass('ccn--identify--home-page-10') }}>
  {% block content %}


  <section id=\"banner-section\" class=\"banner-section clearfix\">
    <div class=\"sass-banner-4 clearfix\">

      <div class=\"container\">
        <div class=\"row justify-content-lg-between justify-content-md-center\">

          <div class=\"col-lg-7 col-md-8 col-sm-12\">
            <!-- show on mobile device - start -->
            <div class=\"mobile-banner-image d-none\">
              <img src=\"{{ base_path ~ directory }}/images/mobile-banner/img-10.png\" alt=\"image_not_found\">
            </div>
            <!-- show on mobile device - end -->
            <div class=\"banner-content\">
              <h1 class=\"title-text mb-30\">
                {{ title_prefix }}
                {% if label %}
                  {{ label }}
                {% endif %}
                {{ title_suffix }}

              </h1>
              <p>
                {{ content.field_body_plain }}
              </p>
              <div class=\"btns-group ul-li clearfix\">
                <ul class=\"clearfix\">
                  <li><a href=\"{{ content.field_link.0['#url'] }}\" class=\"custom-btn\">{{ content.field_link.0['#title'] }}</a></li>
                  <li>
                    <p class=\"info-text mb-0\">{{ content.field_subtitle }}</p>
                  </li>
                </ul>
              </div>
            </div>
          </div>

          <div class=\"col-lg-5\">
            <div class=\"banner-item-image\">
              <span class=\"laptop-image\" data-aos=\"fade-left\" data-aos-delay=\"100\">
                <img src=\"{{ file_url(content.field_image[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
              </span>
              <span class=\"bg-image\">
                <img src=\"{{ base_path ~ directory }}/images/banner/shape-3.png\" alt=\"image_not_found\">
              </span>
              <span class=\"shape-image\" data-aos=\"zoom-in\" data-aos-delay=\"300\">
                <img src=\"{{ base_path ~ directory }}/images/banner/shape-4.png\" alt=\"image_not_found\">
              </span>
              <a class=\"popup-video\" href=\"{{ content.field_youtube_video_embed.0 }}\" data-aos=\"zoom-in\" data-aos-delay=\"900\">
                <i class='uil uil-play'></i>
              </a>
            </div>
          </div>

        </div>
      </div>

      <span class=\"shape-1\">
        <img src=\"{{ base_path ~ directory }}/images/shapes/cross-2.png\" alt=\"image_not_found\">
      </span>
      <span class=\"shape-2\">
        <img src=\"{{ base_path ~ directory }}/images/shapes/flow-1-2.png\" alt=\"image_not_found\">
      </span>
      <span class=\"shape-3\">
        <img src=\"{{ base_path ~ directory }}/images/shapes/circle-half-2.png\" alt=\"image_not_found\">
      </span>
      <span class=\"shape-4\">
        <img src=\"{{ base_path ~ directory }}/images/shapes/circle-half-2.png\" alt=\"image_not_found\">
      </span>
      <span class=\"shape-5\">
        <img src=\"{{ base_path ~ directory }}/images/shapes/box-2.png\" alt=\"image_not_found\">
      </span>
      <span class=\"shape-6\">
        <img src=\"{{ base_path ~ directory }}/images/shapes/flow-2-2.png\" alt=\"image_not_found\">
      </span>

    </div>
  </section>


  {% endblock %}
</div>
", "themes/appal/templates/blocks/cocoon/hero-saas/block--hero-saas-modern.html.twig", "/home/princewill/Projects/Drupal Projects/drupal/themes/appal/templates/blocks/cocoon/hero-saas/block--hero-saas-modern.html.twig");
    }
}
