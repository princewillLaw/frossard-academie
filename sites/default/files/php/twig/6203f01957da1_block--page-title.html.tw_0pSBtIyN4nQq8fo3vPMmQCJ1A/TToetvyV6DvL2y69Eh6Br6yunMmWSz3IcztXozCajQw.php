<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/appal/templates/blocks/cocoon/block--page-title.html.twig */
class __TwigTemplate_b718084b49dbf4795107a98cf6db596530dbc6d5b52a07cd3384e1ae464cb6d1 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 2, "block" => 11, "if" => 46];
        $filters = ["clean_class" => 4, "escape" => 10];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'block', 'if'],
                ['clean_class', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        $context["classes"] = [0 => "block", 1 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed($this->getAttribute(        // line 4
($context["configuration"] ?? null), "provider", [])))), 2 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 5
($context["plugin_id"] ?? null)))), 3 => ((        // line 6
($context["label"] ?? null)) ? ("has-title") : ("")), 4 => (($this->getAttribute($this->getAttribute(        // line 7
($context["content"] ?? null), "body", [], "array"), "#bundle", [], "array")) ? (("bundle-" . $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["content"] ?? null), "body", [], "array"), "#bundle", [], "array")))) : (""))];
        // line 10
        echo "<div";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method"), "addClass", [0 => "ccn--identify--page-title"], "method")), "html", null, true);
        echo ">
  ";
        // line 11
        $this->displayBlock('content', $context, $blocks);
        // line 95
        echo "</div>
";
    }

    // line 11
    public function block_content($context, array $blocks = [])
    {
        // line 12
        echo "    <section id=\"breadcrumb-section\" class=\"breadcrumb-section mma-container clearfix\" style=\"background-image: url(";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/breadcrumb/bg-image-1.jpg);\">
      <div class=\"design-image-1 clearfix\">
        <small class=\"image-1\" data-aos=\"fade-up\" data-aos-delay=\"100\">
          <img src=\"";
        // line 15
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/breadcrumb/design-image-1.png\" alt=\"image_not_found\">
        </small>
        <small class=\"man-image-1\" data-aos=\"fade-down\" data-aos-delay=\"300\">
          <img src=\"";
        // line 18
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/breadcrumb/img-1.png\" alt=\"image_not_found\">
        </small>
        <small class=\"man-image-2\" data-aos=\"fade-down\" data-aos-delay=\"600\">
          <img src=\"";
        // line 21
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/breadcrumb/img-2.png\" alt=\"image_not_found\">
        </small>
        <small class=\"shape-image-1\" data-aos=\"zoom-in\" data-aos-delay=\"1000\">
          <img src=\"";
        // line 24
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/breadcrumb/img-5.png\" alt=\"image_not_found\">
        </small>
        <small class=\"medal-image-1\" data-aos=\"flip-left\" data-aos-delay=\"1400\">
          <img src=\"";
        // line 27
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/breadcrumb/img-3.png\" alt=\"image_not_found\">
        </small>
        <small class=\"shape-image-2\" data-aos=\"zoom-in\" data-aos-delay=\"1100\">
          <img src=\"";
        // line 30
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/breadcrumb/img-5.png\" alt=\"image_not_found\">
        </small>
        <small class=\"medal-image-2\" data-aos=\"flip-left\" data-aos-delay=\"1500\">
          <img src=\"";
        // line 33
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/breadcrumb/img-4.png\" alt=\"image_not_found\">
        </small>
      </div>
      <small class=\"design-image-2\" data-aos=\"fade-up\" data-aos-delay=\"200\">
        <img src=\"";
        // line 37
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/breadcrumb/design-image-2.png\" alt=\"image_not_found\">
      </small>
      <div class=\"container\">
        <div class=\"row justify-content-lg-start justify-content-md-center\">
          <div class=\"col-lg-5 col-md-6 col-sm-12\">
            <div class=\"breadcrumb-content\">
              <span class=\"page-name\">";
        // line 43
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_subtitle", [])), "html", null, true);
        echo "</span>
              <div class=\"section-title\">
                <h2 class=\"title-text mb-15\">";
        // line 45
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo "
                  ";
        // line 46
        if (($context["label"] ?? null)) {
            // line 47
            echo "                    ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null)), "html", null, true);
            echo "
                  ";
        }
        // line 49
        echo "                  ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "</h2>
                <p class=\"paragraph-text mb-0\">
                  ";
        // line 51
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_body_plain", [])), "html", null, true);
        echo "
                </p>
              </div>
              <nav class=\"breadcrumb-nav\" aria-label=\"breadcrumb\">
                <ol class=\"breadcrumb\">
                  <li class=\"breadcrumb-item\">
                    <a href=\"";
        // line 57
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)), "html", null, true);
        echo "\">home</a>
                  </li>
                  <li class=\"breadcrumb-item active\" aria-current=\"page\">";
        // line 59
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null)), "html", null, true);
        echo "</li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
      <small class=\"cross-image-1 spin-image mma-item\">
        <img src=\"";
        // line 67
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/shapes/cross.png\" alt=\"image_not_found\">
      </small>
      <small class=\"cross-image-2 spin-image mma-item\">
        <img src=\"";
        // line 70
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/shapes/cross.png\" alt=\"image_not_found\">
      </small>
      <small class=\"box-image-1 spin-image mma-item\">
        <img src=\"";
        // line 73
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/shapes/box.png\" alt=\"image_not_found\">
      </small>
      <small class=\"box-image-2 spin-image mma-item\">
        <img src=\"";
        // line 76
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/shapes/box.png\" alt=\"image_not_found\">
      </small>
      <small class=\"flow-image-1 zoominout-image mma-item\">
        <img src=\"";
        // line 79
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/shapes/flow-1.png\" alt=\"image_not_found\">
      </small>
      <small class=\"flow-image-2 zoominout-image mma-item\">
        <img src=\"";
        // line 82
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/shapes/flow-2.png\" alt=\"image_not_found\">
      </small>
      <small class=\"circle-half-1 spin-image mma-item\">
        <img src=\"";
        // line 85
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/shapes/circle-half.png\" alt=\"image_not_found\">
      </small>
      <small class=\"circle-half-2 spin-image mma-item\">
        <img src=\"";
        // line 88
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/shapes/circle-half.png\" alt=\"image_not_found\">
      </small>
      <small class=\"circle-image-1 zoominout-image mma-item\">
        <img src=\"";
        // line 91
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/shapes/circle.png\" alt=\"image_not_found\">
      </small>
    </section>
  ";
    }

    public function getTemplateName()
    {
        return "themes/appal/templates/blocks/cocoon/block--page-title.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  232 => 91,  226 => 88,  220 => 85,  214 => 82,  208 => 79,  202 => 76,  196 => 73,  190 => 70,  184 => 67,  173 => 59,  168 => 57,  159 => 51,  153 => 49,  147 => 47,  145 => 46,  141 => 45,  136 => 43,  127 => 37,  120 => 33,  114 => 30,  108 => 27,  102 => 24,  96 => 21,  90 => 18,  84 => 15,  77 => 12,  74 => 11,  69 => 95,  67 => 11,  62 => 10,  60 => 7,  59 => 6,  58 => 5,  57 => 4,  56 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{%
  set classes = [
    'block',
    'block-' ~ configuration.provider|clean_class,
    'block-' ~ plugin_id|clean_class,
    label ? 'has-title',
    content['body']['#bundle'] ? 'bundle-' ~ content['body']['#bundle'],
  ]
%}
<div{{ attributes.addClass(classes).addClass('ccn--identify--page-title') }}>
  {% block content %}
    <section id=\"breadcrumb-section\" class=\"breadcrumb-section mma-container clearfix\" style=\"background-image: url({{ base_path ~ directory }}/images/breadcrumb/bg-image-1.jpg);\">
      <div class=\"design-image-1 clearfix\">
        <small class=\"image-1\" data-aos=\"fade-up\" data-aos-delay=\"100\">
          <img src=\"{{ base_path ~ directory }}/images/breadcrumb/design-image-1.png\" alt=\"image_not_found\">
        </small>
        <small class=\"man-image-1\" data-aos=\"fade-down\" data-aos-delay=\"300\">
          <img src=\"{{ base_path ~ directory }}/images/breadcrumb/img-1.png\" alt=\"image_not_found\">
        </small>
        <small class=\"man-image-2\" data-aos=\"fade-down\" data-aos-delay=\"600\">
          <img src=\"{{ base_path ~ directory }}/images/breadcrumb/img-2.png\" alt=\"image_not_found\">
        </small>
        <small class=\"shape-image-1\" data-aos=\"zoom-in\" data-aos-delay=\"1000\">
          <img src=\"{{ base_path ~ directory }}/images/breadcrumb/img-5.png\" alt=\"image_not_found\">
        </small>
        <small class=\"medal-image-1\" data-aos=\"flip-left\" data-aos-delay=\"1400\">
          <img src=\"{{ base_path ~ directory }}/images/breadcrumb/img-3.png\" alt=\"image_not_found\">
        </small>
        <small class=\"shape-image-2\" data-aos=\"zoom-in\" data-aos-delay=\"1100\">
          <img src=\"{{ base_path ~ directory }}/images/breadcrumb/img-5.png\" alt=\"image_not_found\">
        </small>
        <small class=\"medal-image-2\" data-aos=\"flip-left\" data-aos-delay=\"1500\">
          <img src=\"{{ base_path ~ directory }}/images/breadcrumb/img-4.png\" alt=\"image_not_found\">
        </small>
      </div>
      <small class=\"design-image-2\" data-aos=\"fade-up\" data-aos-delay=\"200\">
        <img src=\"{{ base_path ~ directory }}/images/breadcrumb/design-image-2.png\" alt=\"image_not_found\">
      </small>
      <div class=\"container\">
        <div class=\"row justify-content-lg-start justify-content-md-center\">
          <div class=\"col-lg-5 col-md-6 col-sm-12\">
            <div class=\"breadcrumb-content\">
              <span class=\"page-name\">{{ content.field_subtitle }}</span>
              <div class=\"section-title\">
                <h2 class=\"title-text mb-15\">{{ title_prefix }}
                  {% if label %}
                    {{ label }}
                  {% endif %}
                  {{ title_suffix }}</h2>
                <p class=\"paragraph-text mb-0\">
                  {{ content.field_body_plain }}
                </p>
              </div>
              <nav class=\"breadcrumb-nav\" aria-label=\"breadcrumb\">
                <ol class=\"breadcrumb\">
                  <li class=\"breadcrumb-item\">
                    <a href=\"{{ base_path }}\">home</a>
                  </li>
                  <li class=\"breadcrumb-item active\" aria-current=\"page\">{{ label }}</li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
      <small class=\"cross-image-1 spin-image mma-item\">
        <img src=\"{{ base_path ~ directory }}/images/shapes/cross.png\" alt=\"image_not_found\">
      </small>
      <small class=\"cross-image-2 spin-image mma-item\">
        <img src=\"{{ base_path ~ directory }}/images/shapes/cross.png\" alt=\"image_not_found\">
      </small>
      <small class=\"box-image-1 spin-image mma-item\">
        <img src=\"{{ base_path ~ directory }}/images/shapes/box.png\" alt=\"image_not_found\">
      </small>
      <small class=\"box-image-2 spin-image mma-item\">
        <img src=\"{{ base_path ~ directory }}/images/shapes/box.png\" alt=\"image_not_found\">
      </small>
      <small class=\"flow-image-1 zoominout-image mma-item\">
        <img src=\"{{ base_path ~ directory }}/images/shapes/flow-1.png\" alt=\"image_not_found\">
      </small>
      <small class=\"flow-image-2 zoominout-image mma-item\">
        <img src=\"{{ base_path ~ directory }}/images/shapes/flow-2.png\" alt=\"image_not_found\">
      </small>
      <small class=\"circle-half-1 spin-image mma-item\">
        <img src=\"{{ base_path ~ directory }}/images/shapes/circle-half.png\" alt=\"image_not_found\">
      </small>
      <small class=\"circle-half-2 spin-image mma-item\">
        <img src=\"{{ base_path ~ directory }}/images/shapes/circle-half.png\" alt=\"image_not_found\">
      </small>
      <small class=\"circle-image-1 zoominout-image mma-item\">
        <img src=\"{{ base_path ~ directory }}/images/shapes/circle.png\" alt=\"image_not_found\">
      </small>
    </section>
  {% endblock %}
</div>
", "themes/appal/templates/blocks/cocoon/block--page-title.html.twig", "/home/princewill/Projects/Drupal Projects/drupal/themes/appal/templates/blocks/cocoon/block--page-title.html.twig");
    }
}
