<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/appal/templates/views/cocoon-content-slider/views-view-fields--cocoon-content-slider.html.twig */
class __TwigTemplate_a737f3fbd0e187afdb2618586fac52bcf33de298d7b4b347aa1a51d8974c6e24 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 4, "convert_encoding" => 4, "striptags" => 4];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape', 'convert_encoding', 'striptags'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"item\">
  <div class=\"blog-grid-item clearfix\">
    <div class=\"blog-image\">
      <img src=\"";
        // line 4
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_convert_encoding(strip_tags($this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["fields"] ?? null), "field_media_image", []), "content", []))), "UTF-8", "HTML-ENTITIES"), "html", null, true);
        echo "\" alt=\"image_not_found\">
      <div class=\"post-date\">
        <strong class=\"date-text\">";
        // line 6
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["fields"] ?? null), "created_2", []), "content", [])), "html", null, true);
        echo "</strong>
        <span class=\"month-text\">";
        // line 7
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["fields"] ?? null), "created_1", []), "content", [])), "html", null, true);
        echo "</span>
      </div>
    </div>
    <div class=\"blog-content\">
      <div class=\"post-meta ul-li mb-30 clearfix\">
        <ul class=\"clearfix\">
          <li>
            <a href=\"#!\" class=\"post-admin\">
              <span class=\"admin-image\"><img src=\"";
        // line 15
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_convert_encoding(strip_tags($this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["fields"] ?? null), "user_picture", []), "content", []))), "UTF-8", "HTML-ENTITIES"), "html", null, true);
        echo "\" alt=\"image_not_found\"></span>
              ";
        // line 16
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["fields"] ?? null), "uid", []), "content", [])), "html", null, true);
        echo "
            </a>
          </li>
          <li>
            <i class='uil uil-stopwatch'></i>
            ";
        // line 21
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["fields"] ?? null), "created", []), "content", [])), "html", null, true);
        echo "</li>
        </ul>
      </div>
      ";
        // line 24
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["fields"] ?? null), "field_categories", []), "content", [])), "html", null, true);
        echo "
      <h3 class=\"blog-title mb-0\">
        <a href=\"";
        // line 26
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_convert_encoding(strip_tags($this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["fields"] ?? null), "view_node", []), "content", []))), "UTF-8", "HTML-ENTITIES"), "html", null, true);
        echo "\" class=\"title-link\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["fields"] ?? null), "title", []), "content", [])), "html", null, true);
        echo "</a>
      </h3>
    </div>
  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "themes/appal/templates/views/cocoon-content-slider/views-view-fields--cocoon-content-slider.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 26,  98 => 24,  92 => 21,  84 => 16,  80 => 15,  69 => 7,  65 => 6,  60 => 4,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"item\">
  <div class=\"blog-grid-item clearfix\">
    <div class=\"blog-image\">
      <img src=\"{{ fields.field_media_image.content|striptags|convert_encoding('UTF-8', 'HTML-ENTITIES') }}\" alt=\"image_not_found\">
      <div class=\"post-date\">
        <strong class=\"date-text\">{{ fields.created_2.content }}</strong>
        <span class=\"month-text\">{{ fields.created_1.content }}</span>
      </div>
    </div>
    <div class=\"blog-content\">
      <div class=\"post-meta ul-li mb-30 clearfix\">
        <ul class=\"clearfix\">
          <li>
            <a href=\"#!\" class=\"post-admin\">
              <span class=\"admin-image\"><img src=\"{{ fields.user_picture.content|striptags|convert_encoding('UTF-8', 'HTML-ENTITIES') }}\" alt=\"image_not_found\"></span>
              {{ fields.uid.content }}
            </a>
          </li>
          <li>
            <i class='uil uil-stopwatch'></i>
            {{ fields.created.content }}</li>
        </ul>
      </div>
      {{ fields.field_categories.content }}
      <h3 class=\"blog-title mb-0\">
        <a href=\"{{ fields.view_node.content|striptags|convert_encoding('UTF-8', 'HTML-ENTITIES') }}\" class=\"title-link\">{{ fields.title.content }}</a>
      </h3>
    </div>
  </div>
</div>
", "themes/appal/templates/views/cocoon-content-slider/views-view-fields--cocoon-content-slider.html.twig", "/home/princewill/Projects/Drupal Projects/drupal/themes/appal/templates/views/cocoon-content-slider/views-view-fields--cocoon-content-slider.html.twig");
    }
}
