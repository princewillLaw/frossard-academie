<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/appal/templates/blocks/cocoon/block--category-faqs.html.twig */
class __TwigTemplate_5a5b9f314a2438624463caccbd18542abac39b0db50d1d1bca00dfd4114474cb extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 2, "block" => 11, "if" => 19, "for" => 34];
        $filters = ["clean_class" => 4, "escape" => 10, "length" => 33];
        $functions = ["range" => 34];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'block', 'if', 'for'],
                ['clean_class', 'escape', 'length'],
                ['range']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        $context["classes"] = [0 => "block", 1 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed($this->getAttribute(        // line 4
($context["configuration"] ?? null), "provider", [])))), 2 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 5
($context["plugin_id"] ?? null)))), 3 => ((        // line 6
($context["label"] ?? null)) ? ("has-title") : ("")), 4 => (($this->getAttribute($this->getAttribute(        // line 7
($context["content"] ?? null), "body", [], "array"), "#bundle", [], "array")) ? (("bundle-" . $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["content"] ?? null), "body", [], "array"), "#bundle", [], "array")))) : (""))];
        // line 10
        echo "<div";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
        echo ">
  ";
        // line 11
        $this->displayBlock('content', $context, $blocks);
        // line 54
        echo "</div>
";
    }

    // line 11
    public function block_content($context, array $blocks = [])
    {
        // line 12
        echo "    <section id=\"faq-section\" class=\"faq-section sec-ptb-160 bg-light-gray clearfix\">
      <div class=\"container\">
        <div class=\"row justify-content-center\">
          <div class=\"col-lg-7 col-md-8 col-sm-12\">
            <div class=\"section-title mb-100 text-center\" data-aos=\"fade-up\" data-aos-duration=\"450\" data-aos-delay=\"100\">
              <span class=\"sub-title mb-15\">";
        // line 17
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_supertitle", [])), "html", null, true);
        echo "</span>
              <h2 class=\"title-text mb-30\">";
        // line 18
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo "
                ";
        // line 19
        if (($context["label"] ?? null)) {
            // line 20
            echo "                  ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null)), "html", null, true);
            echo "
                ";
        }
        // line 22
        echo "                ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "</h2>
              <p class=\"paragraph-text mb-0\">
                ";
        // line 24
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_subtitle", [])), "html", null, true);
        echo "
              </p>
            </div>
          </div>
        </div>
        <div class=\"row justify-content-lg-start justify-content-md-center\">
          <div class=\"col-lg-4 col-md-6 col-sm-12\">
            <div class=\"faq-tabs-nav ul-li-block clearfix\" data-aos=\"fade-right\" data-aos-duration=\"450\" data-aos-delay=\"300\">
              <ul class=\"nav\" role=\"tablist\">
                ";
        // line 33
        $context["numTabsIndex"] = twig_length_filter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_paragraphs", []), "#items", [], "array")));
        // line 34
        echo "                ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(0, ($context["numTabsIndex"] ?? null)));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 35
            echo "                  ";
            if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_paragraphs", []), $context["i"], [], "array"), "#paragraph", [], "array"), "id", [], "method")) {
                // line 36
                echo "                    <li class=\"nav-item\">
                      <a class=\"nav-link\" data-toggle=\"tab\" href=\"#tab-";
                // line 37
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_paragraphs", []), $context["i"], [], "array"), "#paragraph", [], "array"), "id", [], "method")), "html", null, true);
                echo "\">";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_paragraphs", []), $context["i"], [], "array"), "#paragraph", [], "array"), "field_title", []), "value", [])), "html", null, true);
                echo "</a>
                    </li>
                  ";
            }
            // line 40
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "              </ul>
              ";
        // line 42
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "body", [])), "html", null, true);
        echo "
            </div>
          </div>
          <div class=\"col-lg-8 col-md-8 col-sm-12\">
            <div class=\"tab-content\">
              ";
        // line 47
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_paragraphs", [])), "html", null, true);
        echo "
            </div>
          </div>
        </div>
      </div>
    </section>
  ";
    }

    public function getTemplateName()
    {
        return "themes/appal/templates/blocks/cocoon/block--category-faqs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  156 => 47,  148 => 42,  145 => 41,  139 => 40,  131 => 37,  128 => 36,  125 => 35,  120 => 34,  118 => 33,  106 => 24,  100 => 22,  94 => 20,  92 => 19,  88 => 18,  84 => 17,  77 => 12,  74 => 11,  69 => 54,  67 => 11,  62 => 10,  60 => 7,  59 => 6,  58 => 5,  57 => 4,  56 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{%
  set classes = [
    'block',
    'block-' ~ configuration.provider|clean_class,
    'block-' ~ plugin_id|clean_class,
    label ? 'has-title',
    content['body']['#bundle'] ? 'bundle-' ~ content['body']['#bundle'],
  ]
%}
<div{{ attributes.addClass(classes) }}>
  {% block content %}
    <section id=\"faq-section\" class=\"faq-section sec-ptb-160 bg-light-gray clearfix\">
      <div class=\"container\">
        <div class=\"row justify-content-center\">
          <div class=\"col-lg-7 col-md-8 col-sm-12\">
            <div class=\"section-title mb-100 text-center\" data-aos=\"fade-up\" data-aos-duration=\"450\" data-aos-delay=\"100\">
              <span class=\"sub-title mb-15\">{{ content.field_supertitle }}</span>
              <h2 class=\"title-text mb-30\">{{ title_prefix }}
                {% if label %}
                  {{ label }}
                {% endif %}
                {{ title_suffix }}</h2>
              <p class=\"paragraph-text mb-0\">
                {{ content.field_subtitle  }}
              </p>
            </div>
          </div>
        </div>
        <div class=\"row justify-content-lg-start justify-content-md-center\">
          <div class=\"col-lg-4 col-md-6 col-sm-12\">
            <div class=\"faq-tabs-nav ul-li-block clearfix\" data-aos=\"fade-right\" data-aos-duration=\"450\" data-aos-delay=\"300\">
              <ul class=\"nav\" role=\"tablist\">
                {% set numTabsIndex = content.field_paragraphs['#items'] | length %}
                {% for i in 0..numTabsIndex %}
                  {% if content.field_paragraphs[i]['#paragraph'].id() %}
                    <li class=\"nav-item\">
                      <a class=\"nav-link\" data-toggle=\"tab\" href=\"#tab-{{ content.field_paragraphs[i]['#paragraph'].id() }}\">{{ content.field_paragraphs[i]['#paragraph'].field_title.value }}</a>
                    </li>
                  {% endif %}
                {% endfor %}
              </ul>
              {{ content.body}}
            </div>
          </div>
          <div class=\"col-lg-8 col-md-8 col-sm-12\">
            <div class=\"tab-content\">
              {{ content.field_paragraphs }}
            </div>
          </div>
        </div>
      </div>
    </section>
  {% endblock %}
</div>
", "themes/appal/templates/blocks/cocoon/block--category-faqs.html.twig", "/home/princewill/Projects/Drupal Projects/drupal/themes/appal/templates/blocks/cocoon/block--category-faqs.html.twig");
    }
}
