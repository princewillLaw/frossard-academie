<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/appal/templates/status-messages.html.twig */
class __TwigTemplate_0cf53441a56ba52c1ac399085f271cd28dab04ce76b12813b01e6203f4056269 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["for" => 2, "if" => 3];
        $filters = ["escape" => 3, "without" => 3, "length" => 11, "first" => 18];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['for', 'if'],
                ['escape', 'without', 'length', 'first'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"ccn-message-container\">
  ";
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["message_list"] ?? null));
        foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
            // line 3
            echo "    <div class=\"ccn-message";
            if (($context["type"] == "error")) {
                echo " ccn-message-error ";
            } else {
                echo " ccn-message-status";
            }
            echo "\" role=\"contentinfo\" aria-label=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["status_headings"] ?? null), $context["type"], [], "array")), "html", null, true);
            echo "\" ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->withoutFilter($this->sandbox->ensureToStringAllowed(($context["attributes"] ?? null)), "role", "aria-label"), "html", null, true);
            echo ">
      <div class=\"ccn-message-content\">
        ";
            // line 5
            if (($context["type"] == "error")) {
                // line 6
                echo "          <div role=\"alert\">
          ";
            }
            // line 8
            echo "          ";
            if ($this->getAttribute(($context["status_headings"] ?? null), $context["type"], [], "array")) {
                // line 9
                echo "            <h2 class=\"visually-hidden\">";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["status_headings"] ?? null), $context["type"], [], "array")), "html", null, true);
                echo "</h2>
          ";
            }
            // line 11
            echo "          ";
            if ((twig_length_filter($this->env, $context["messages"]) > 1)) {
                // line 12
                echo "            <ul>
              ";
                // line 13
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 14
                    echo "                <li>";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["message"]), "html", null, true);
                    echo "</li>
              ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 16
                echo "            </ul>
          ";
            } else {
                // line 18
                echo "            ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_first($this->env, $this->sandbox->ensureToStringAllowed($context["messages"])), "html", null, true);
                echo "
          ";
            }
            // line 20
            echo "          ";
            if (($context["type"] == "error")) {
                // line 21
                echo "          </div>
        ";
            }
            // line 23
            echo "      </div>
      <div class=\"ccn-message-action\">
        <button tabindex=\"0\" type=\"button\" aria-label=\"close\">
          <span>
            <svg focusable=\"false\" viewbox=\"0 0 24 24\" aria-hidden=\"true\" role=\"presentation\">
              <path d=\"M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z\"></path>
            </svg>
          </span>
        </button>
      </div>
    </div>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "themes/appal/templates/status-messages.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  144 => 35,  127 => 23,  123 => 21,  120 => 20,  114 => 18,  110 => 16,  101 => 14,  97 => 13,  94 => 12,  91 => 11,  85 => 9,  82 => 8,  78 => 6,  76 => 5,  62 => 3,  58 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"ccn-message-container\">
  {% for type, messages in message_list %}
    <div class=\"ccn-message{% if type == 'error' %} ccn-message-error {% else %} ccn-message-status{% endif %}\" role=\"contentinfo\" aria-label=\"{{ status_headings[type] }}\" {{ attributes|without('role', 'aria-label') }}>
      <div class=\"ccn-message-content\">
        {% if type == 'error' %}
          <div role=\"alert\">
          {% endif %}
          {% if status_headings[type] %}
            <h2 class=\"visually-hidden\">{{ status_headings[type] }}</h2>
          {% endif %}
          {% if messages | length > 1 %}
            <ul>
              {% for message in messages %}
                <li>{{ message }}</li>
              {% endfor %}
            </ul>
          {% else %}
            {{ messages|first }}
          {% endif %}
          {% if type == 'error' %}
          </div>
        {% endif %}
      </div>
      <div class=\"ccn-message-action\">
        <button tabindex=\"0\" type=\"button\" aria-label=\"close\">
          <span>
            <svg focusable=\"false\" viewbox=\"0 0 24 24\" aria-hidden=\"true\" role=\"presentation\">
              <path d=\"M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z\"></path>
            </svg>
          </span>
        </button>
      </div>
    </div>
  {% endfor %}
</div>", "themes/appal/templates/status-messages.html.twig", "/home/princewill/Projects/Drupal Projects/drupal/themes/appal/templates/status-messages.html.twig");
    }
}
