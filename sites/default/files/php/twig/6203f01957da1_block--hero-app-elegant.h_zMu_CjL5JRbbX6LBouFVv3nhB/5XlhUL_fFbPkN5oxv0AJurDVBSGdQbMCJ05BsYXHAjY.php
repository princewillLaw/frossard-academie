<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/appal/templates/blocks/cocoon/hero-app/block--hero-app-elegant.html.twig */
class __TwigTemplate_2def28f00158d0da16fd3a43a95d57f37c53be8896b9d4dce8f676c2de862823 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 2, "block" => 11, "if" => 43];
        $filters = ["clean_class" => 4, "escape" => 10];
        $functions = ["file_url" => 13];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'block', 'if'],
                ['clean_class', 'escape'],
                ['file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        $context["classes"] = [0 => "block", 1 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed($this->getAttribute(        // line 4
($context["configuration"] ?? null), "provider", [])))), 2 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 5
($context["plugin_id"] ?? null)))), 3 => ((        // line 6
($context["label"] ?? null)) ? ("has-title") : ("")), 4 => (($this->getAttribute($this->getAttribute(        // line 7
($context["content"] ?? null), "body", [], "array"), "#bundle", [], "array")) ? (("bundle-" . $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["content"] ?? null), "body", [], "array"), "#bundle", [], "array")))) : (""))];
        // line 10
        echo "<div";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
        echo ">
  ";
        // line 11
        $this->displayBlock('content', $context, $blocks);
        // line 95
        echo "</div>
";
    }

    // line 11
    public function block_content($context, array $blocks = [])
    {
        // line 12
        echo "    <section id=\"banner-section\" class=\"banner-section clearfix\">
      <div class=\"mobile-app-banner-2\" style=\"background-image: url(";
        // line 13
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo ");\">
        <span class=\"shape-image-1\" data-aos=\"fade-right\" data-aos-duration=\"1000\" data-delay=\"100\">
          <img src=\"";
        // line 15
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/banner/shape-5.png\" alt=\"image_not_found\">
        </span>
        <span class=\"shape-image-2\">
          <small data-aos=\"zoom-in\" data-aos-duration=\"1000\" data-delay=\"800\">
            <img src=\"";
        // line 19
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/banner/shape-6.png\" alt=\"image_not_found\">
          </small>
        </span>
        <span class=\"shape-image-3\">
          <small data-aos=\"zoom-out\" data-aos-duration=\"1000\" data-delay=\"1000\">
            <img src=\"";
        // line 24
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/banner/shape-7.png\" alt=\"image_not_found\">
          </small>
        </span>
        <span class=\"shape-image-4\">
          <small data-aos=\"zoom-out\" data-aos-duration=\"1000\" data-delay=\"1200\">
            <img src=\"";
        // line 29
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/banner/shape-8.png\" alt=\"image_not_found\">
          </small>
        </span>
        <div class=\"container\">
          <div class=\"row justify-content-lg-start justify-content-md-center\">
            <div class=\"col-lg-6 col-md-8 col-sm-12\">
              <!-- show on mobile device - start -->
              <div class=\"mobile-banner-image d-none\">
                <img src=\"";
        // line 37
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/mobile-banner/img-2.png\" alt=\"image_not_found\">
              </div>
              <!-- show on mobile device - end -->
              <div class=\"banner-content\">
                <h2>
                  ";
        // line 42
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo "
                  ";
        // line 43
        if (($context["label"] ?? null)) {
            // line 44
            echo "                    ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null)), "html", null, true);
            echo "
                  ";
        }
        // line 46
        echo "                  ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "
                </h2>
                <p>
                  ";
        // line 49
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_body_plain", [])), "html", null, true);
        echo "
                </p>
                <div class=\"btns-group ul-li clearfix\">
                  <ul class=\"clearfix\">
                    ";
        // line 53
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_paragraphs", [])), "html", null, true);
        echo "
                  </ul>
                </div>
              </div>
            </div>
            <div class=\"col-lg-6 col-md-8 col-sm-12\">
              <div class=\"banner-image scene\">
                <span class=\"phone-image\" data-aos=\"zoom-out\" data-aos-duration=\"1000\" data-aos-delay=\"200\">
                  <img src=\"";
        // line 61
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_2", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
                </span>
                <span class=\"man-image-1\" data-depth=\"0.1\">
                  <small data-aos=\"fade-right\" data-aos-duration=\"500\" data-aos-delay=\"500\">
                    <img src=\"";
        // line 65
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_3", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
                  </small>
                </span>
                <span class=\"man-image-2\" data-depth=\"0.1\">
                  <small data-aos=\"fade-left\" data-aos-duration=\"500\" data-aos-delay=\"600\">
                    <img src=\"";
        // line 70
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_4", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
                  </small>
                </span>
                <span class=\"message-image\" data-depth=\"0.2\">
                  <small data-aos=\"fade-down\" data-aos-duration=\"1200\" data-aos-delay=\"900\">
                    <img src=\"";
        // line 75
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_5", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
                  </small>
                </span>
                <span class=\"leaf-image-1\" data-depth=\"0.2\">
                  <small data-aos=\"fade-right\" data-aos-duration=\"1000\" data-aos-delay=\"1200\">
                    <img src=\"";
        // line 80
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_6", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
                  </small>
                </span>
                <span class=\"leaf-image-2\" data-depth=\"0.2\">
                  <small data-aos=\"fade-left\" data-aos-duration=\"1000\" data-aos-delay=\"1300\">
                    <img src=\"";
        // line 85
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_7", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
                  </small>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  ";
    }

    public function getTemplateName()
    {
        return "themes/appal/templates/blocks/cocoon/hero-app/block--hero-app-elegant.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  203 => 85,  195 => 80,  187 => 75,  179 => 70,  171 => 65,  164 => 61,  153 => 53,  146 => 49,  139 => 46,  133 => 44,  131 => 43,  127 => 42,  119 => 37,  108 => 29,  100 => 24,  92 => 19,  85 => 15,  80 => 13,  77 => 12,  74 => 11,  69 => 95,  67 => 11,  62 => 10,  60 => 7,  59 => 6,  58 => 5,  57 => 4,  56 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{%
  set classes = [
    'block',
    'block-' ~ configuration.provider|clean_class,
    'block-' ~ plugin_id|clean_class,
    label ? 'has-title',
    content['body']['#bundle'] ? 'bundle-' ~ content['body']['#bundle'],
  ]
%}
<div{{ attributes.addClass(classes) }}>
  {% block content %}
    <section id=\"banner-section\" class=\"banner-section clearfix\">
      <div class=\"mobile-app-banner-2\" style=\"background-image: url({{ file_url(content.field_image[0]['#media'].field_media_image.entity.uri.value) }});\">
        <span class=\"shape-image-1\" data-aos=\"fade-right\" data-aos-duration=\"1000\" data-delay=\"100\">
          <img src=\"{{ base_path ~ directory }}/images/banner/shape-5.png\" alt=\"image_not_found\">
        </span>
        <span class=\"shape-image-2\">
          <small data-aos=\"zoom-in\" data-aos-duration=\"1000\" data-delay=\"800\">
            <img src=\"{{ base_path ~ directory }}/images/banner/shape-6.png\" alt=\"image_not_found\">
          </small>
        </span>
        <span class=\"shape-image-3\">
          <small data-aos=\"zoom-out\" data-aos-duration=\"1000\" data-delay=\"1000\">
            <img src=\"{{ base_path ~ directory }}/images/banner/shape-7.png\" alt=\"image_not_found\">
          </small>
        </span>
        <span class=\"shape-image-4\">
          <small data-aos=\"zoom-out\" data-aos-duration=\"1000\" data-delay=\"1200\">
            <img src=\"{{ base_path ~ directory }}/images/banner/shape-8.png\" alt=\"image_not_found\">
          </small>
        </span>
        <div class=\"container\">
          <div class=\"row justify-content-lg-start justify-content-md-center\">
            <div class=\"col-lg-6 col-md-8 col-sm-12\">
              <!-- show on mobile device - start -->
              <div class=\"mobile-banner-image d-none\">
                <img src=\"{{ base_path ~ directory }}/images/mobile-banner/img-2.png\" alt=\"image_not_found\">
              </div>
              <!-- show on mobile device - end -->
              <div class=\"banner-content\">
                <h2>
                  {{ title_prefix }}
                  {% if label %}
                    {{ label }}
                  {% endif %}
                  {{ title_suffix }}
                </h2>
                <p>
                  {{ content.field_body_plain }}
                </p>
                <div class=\"btns-group ul-li clearfix\">
                  <ul class=\"clearfix\">
                    {{ content.field_paragraphs }}
                  </ul>
                </div>
              </div>
            </div>
            <div class=\"col-lg-6 col-md-8 col-sm-12\">
              <div class=\"banner-image scene\">
                <span class=\"phone-image\" data-aos=\"zoom-out\" data-aos-duration=\"1000\" data-aos-delay=\"200\">
                  <img src=\"{{ file_url(content.field_image_2[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
                </span>
                <span class=\"man-image-1\" data-depth=\"0.1\">
                  <small data-aos=\"fade-right\" data-aos-duration=\"500\" data-aos-delay=\"500\">
                    <img src=\"{{ file_url(content.field_image_3[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
                  </small>
                </span>
                <span class=\"man-image-2\" data-depth=\"0.1\">
                  <small data-aos=\"fade-left\" data-aos-duration=\"500\" data-aos-delay=\"600\">
                    <img src=\"{{ file_url(content.field_image_4[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
                  </small>
                </span>
                <span class=\"message-image\" data-depth=\"0.2\">
                  <small data-aos=\"fade-down\" data-aos-duration=\"1200\" data-aos-delay=\"900\">
                    <img src=\"{{ file_url(content.field_image_5[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
                  </small>
                </span>
                <span class=\"leaf-image-1\" data-depth=\"0.2\">
                  <small data-aos=\"fade-right\" data-aos-duration=\"1000\" data-aos-delay=\"1200\">
                    <img src=\"{{ file_url(content.field_image_6[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
                  </small>
                </span>
                <span class=\"leaf-image-2\" data-depth=\"0.2\">
                  <small data-aos=\"fade-left\" data-aos-duration=\"1000\" data-aos-delay=\"1300\">
                    <img src=\"{{ file_url(content.field_image_7[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
                  </small>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  {% endblock %}
</div>
", "themes/appal/templates/blocks/cocoon/hero-app/block--hero-app-elegant.html.twig", "/home/princewill/Projects/Drupal Projects/drupal/themes/appal/templates/blocks/cocoon/hero-app/block--hero-app-elegant.html.twig");
    }
}
