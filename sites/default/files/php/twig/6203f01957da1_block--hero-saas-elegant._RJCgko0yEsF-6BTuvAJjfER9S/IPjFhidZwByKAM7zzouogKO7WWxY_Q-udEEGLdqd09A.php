<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/appal/templates/blocks/cocoon/hero-saas/block--hero-saas-elegant.html.twig */
class __TwigTemplate_fe92f8c139ceeadc795f105eb4786a0b55c9d8fc73003333083423c311501877 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 2, "block" => 11, "if" => 29];
        $filters = ["clean_class" => 4, "escape" => 10];
        $functions = ["file_url" => 55];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'block', 'if'],
                ['clean_class', 'escape'],
                ['file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        $context["classes"] = [0 => "block", 1 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed($this->getAttribute(        // line 4
($context["configuration"] ?? null), "provider", [])))), 2 => ("block-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 5
($context["plugin_id"] ?? null)))), 3 => ((        // line 6
($context["label"] ?? null)) ? ("has-title") : ("")), 4 => (($this->getAttribute($this->getAttribute(        // line 7
($context["content"] ?? null), "body", [], "array"), "#bundle", [], "array")) ? (("bundle-" . $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["content"] ?? null), "body", [], "array"), "#bundle", [], "array")))) : (""))];
        // line 10
        echo "<div";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
        echo ">
  ";
        // line 11
        $this->displayBlock('content', $context, $blocks);
        // line 136
        echo "</div>
";
    }

    // line 11
    public function block_content($context, array $blocks = [])
    {
        // line 12
        echo "


  <section id=\"banner-section\" class=\"banner-section bg-light-gray clearfix\">
\t\t\t<div class=\"sass-banner-2 clearfix\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<div class=\"row justify-content-lg-between justify-content-md-center\">

\t\t\t\t\t\t<div class=\"col-lg-5 col-md-8 col-sm-12\">
\t\t\t\t\t\t\t<!-- show on mobile device - start -->
\t\t\t\t\t\t\t<div class=\"mobile-banner-image d-none\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 23
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/mobile-banner/img-8.png\" alt=\"image_not_found\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!-- show on mobile device - end -->
\t\t\t\t\t\t\t<div class=\"banner-content\">
\t\t\t\t\t\t\t\t<h2 class=\"title-text mb-30\">
                  ";
        // line 28
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo "
                  ";
        // line 29
        if (($context["label"] ?? null)) {
            // line 30
            echo "                    ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null)), "html", null, true);
            echo "
                  ";
        }
        // line 32
        echo "                  ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "
\t\t\t\t\t\t\t\t</h2>
\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t";
        // line 35
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_body_plain", [])), "html", null, true);
        echo "
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t<div class=\"newsletter-form\">
\t\t\t\t\t\t\t\t\t<form action=\"#\">
\t\t\t\t\t\t\t\t\t\t<div class=\"form-item\">
\t\t\t\t\t\t\t\t\t\t\t";
        // line 40
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_block", [])), "html", null, true);
        echo "
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<p class=\"mb-0\">";
        // line 42
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_subtitle", [])), "html", null, true);
        echo "</p>
\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"col-lg-6 col-md-8 col-sm-12\">
\t\t\t\t\t\t\t<div class=\"banner-item-image scene\">
\t\t\t\t\t\t\t\t<span class=\"bg-image\" data-aos=\"zoom-in\">
\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 51
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null))), "html", null, true);
        echo "/images/banner/sb-2-shape-bg-1.png\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"big-image\" data-depth=\"0.2\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"zoom-out\" data-aos-delay=\"200\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 55
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"man-image-1\" data-depth=\"0.5\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"fade-right\" data-aos-delay=\"500\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 60
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_2", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"man-image-2\" data-depth=\"0.5\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"fade-left\" data-aos-delay=\"700\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 65
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_3", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"man-image-3\" data-depth=\"0.5\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"fade-left\" data-aos-delay=\"800\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 70
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_4", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"man-image-4\" data-depth=\"0.5\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"fade-right\" data-aos-delay=\"900\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 75
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_5", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"man-image-5\" data-depth=\"0.5\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"fade-up\" data-aos-delay=\"1000\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 80
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_6", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"leaf-image-1\" data-depth=\"0.3\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"flip-right\" data-aos-delay=\"800\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 85
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_7", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"leaf-image-2\" data-depth=\"0.3\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"zoom-in\" data-aos-delay=\"1150\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 90
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_8", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"comment-image\" data-depth=\"0.6\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"fade-down\" data-aos-delay=\"1050\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 95
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_9", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"share-image\" data-depth=\"0.6\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"fade-down\" data-aos-delay=\"1100\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 100
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_10", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"cloud-image-1\" data-depth=\"0.2\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"fade-down\" data-aos-delay=\"1200\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 105
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_11", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"cloud-image-2\" data-depth=\"0.3\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"fade-up\" data-aos-delay=\"1300\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 110
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_12", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"cloud-image-3\" data-depth=\"0.3\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"fade-up\" data-aos-delay=\"1350\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 115
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_13", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"cloud-image-4\" data-depth=\"0.3\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"fade-up\" data-aos-delay=\"1400\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 120
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_14", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"cloud-image-5\" data-depth=\"0.3\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"fade-up\" data-aos-delay=\"1450\">
\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 125
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_image_15", []), 0, [], "array"), "#media", [], "array"), "field_media_image", []), "entity", []), "uri", []), "value", []))]), "html", null, true);
        echo "\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</section>
  ";
    }

    public function getTemplateName()
    {
        return "themes/appal/templates/blocks/cocoon/hero-saas/block--hero-saas-elegant.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  261 => 125,  253 => 120,  245 => 115,  237 => 110,  229 => 105,  221 => 100,  213 => 95,  205 => 90,  197 => 85,  189 => 80,  181 => 75,  173 => 70,  165 => 65,  157 => 60,  149 => 55,  142 => 51,  130 => 42,  125 => 40,  117 => 35,  110 => 32,  104 => 30,  102 => 29,  98 => 28,  90 => 23,  77 => 12,  74 => 11,  69 => 136,  67 => 11,  62 => 10,  60 => 7,  59 => 6,  58 => 5,  57 => 4,  56 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{%
  set classes = [
    'block',
    'block-' ~ configuration.provider|clean_class,
    'block-' ~ plugin_id|clean_class,
    label ? 'has-title',
    content['body']['#bundle'] ? 'bundle-' ~ content['body']['#bundle'],
  ]
%}
<div{{ attributes.addClass(classes) }}>
  {% block content %}



  <section id=\"banner-section\" class=\"banner-section bg-light-gray clearfix\">
\t\t\t<div class=\"sass-banner-2 clearfix\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<div class=\"row justify-content-lg-between justify-content-md-center\">

\t\t\t\t\t\t<div class=\"col-lg-5 col-md-8 col-sm-12\">
\t\t\t\t\t\t\t<!-- show on mobile device - start -->
\t\t\t\t\t\t\t<div class=\"mobile-banner-image d-none\">
\t\t\t\t\t\t\t\t<img src=\"{{ base_path ~ directory }}/images/mobile-banner/img-8.png\" alt=\"image_not_found\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!-- show on mobile device - end -->
\t\t\t\t\t\t\t<div class=\"banner-content\">
\t\t\t\t\t\t\t\t<h2 class=\"title-text mb-30\">
                  {{ title_prefix }}
                  {% if label %}
                    {{ label }}
                  {% endif %}
                  {{ title_suffix }}
\t\t\t\t\t\t\t\t</h2>
\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t{{ content.field_body_plain }}
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t<div class=\"newsletter-form\">
\t\t\t\t\t\t\t\t\t<form action=\"#\">
\t\t\t\t\t\t\t\t\t\t<div class=\"form-item\">
\t\t\t\t\t\t\t\t\t\t\t{{ content.field_block }}
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<p class=\"mb-0\">{{ content.field_subtitle }}</p>
\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"col-lg-6 col-md-8 col-sm-12\">
\t\t\t\t\t\t\t<div class=\"banner-item-image scene\">
\t\t\t\t\t\t\t\t<span class=\"bg-image\" data-aos=\"zoom-in\">
\t\t\t\t\t\t\t\t\t<img src=\"{{ base_path ~ directory }}/images/banner/sb-2-shape-bg-1.png\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"big-image\" data-depth=\"0.2\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"zoom-out\" data-aos-delay=\"200\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ file_url(content.field_image[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"man-image-1\" data-depth=\"0.5\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"fade-right\" data-aos-delay=\"500\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ file_url(content.field_image_2[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"man-image-2\" data-depth=\"0.5\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"fade-left\" data-aos-delay=\"700\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ file_url(content.field_image_3[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"man-image-3\" data-depth=\"0.5\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"fade-left\" data-aos-delay=\"800\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ file_url(content.field_image_4[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"man-image-4\" data-depth=\"0.5\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"fade-right\" data-aos-delay=\"900\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ file_url(content.field_image_5[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"man-image-5\" data-depth=\"0.5\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"fade-up\" data-aos-delay=\"1000\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ file_url(content.field_image_6[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"leaf-image-1\" data-depth=\"0.3\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"flip-right\" data-aos-delay=\"800\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ file_url(content.field_image_7[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"leaf-image-2\" data-depth=\"0.3\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"zoom-in\" data-aos-delay=\"1150\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ file_url(content.field_image_8[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"comment-image\" data-depth=\"0.6\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"fade-down\" data-aos-delay=\"1050\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ file_url(content.field_image_9[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"share-image\" data-depth=\"0.6\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"fade-down\" data-aos-delay=\"1100\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ file_url(content.field_image_10[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"cloud-image-1\" data-depth=\"0.2\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"fade-down\" data-aos-delay=\"1200\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ file_url(content.field_image_11[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"cloud-image-2\" data-depth=\"0.3\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"fade-up\" data-aos-delay=\"1300\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ file_url(content.field_image_12[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"cloud-image-3\" data-depth=\"0.3\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"fade-up\" data-aos-delay=\"1350\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ file_url(content.field_image_13[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"cloud-image-4\" data-depth=\"0.3\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"fade-up\" data-aos-delay=\"1400\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ file_url(content.field_image_14[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<span class=\"cloud-image-5\" data-depth=\"0.3\">
\t\t\t\t\t\t\t\t\t<small data-aos=\"fade-up\" data-aos-delay=\"1450\">
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ file_url(content.field_image_15[0]['#media'].field_media_image.entity.uri.value) }}\" alt=\"image_not_found\">
\t\t\t\t\t\t\t\t\t</small>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</section>
  {% endblock %}
</div>
", "themes/appal/templates/blocks/cocoon/hero-saas/block--hero-saas-elegant.html.twig", "/home/princewill/Projects/Drupal Projects/drupal/themes/appal/templates/blocks/cocoon/hero-saas/block--hero-saas-elegant.html.twig");
    }
}
